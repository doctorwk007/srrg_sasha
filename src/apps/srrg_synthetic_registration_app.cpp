#include <qapplication.h>
#include <QGLViewer/qglviewer.h>
#include <QKeyEvent>

#include <srrg_matchable/matchable.h>
#include <srrg_matchable/scene.h>
#include <srrg_sasha/base_registration_solver.h>
#include <srrg_sasha/iterative_registration_solver.h>
#include <srrg_sasha/direct_registration_solver.h>
#include <srrg_gl_helpers/opengl_primitives.h>

#include <srrg_matchable_detector/plane_detector.h>

#include <srrg_system_utils/colors.h>

using namespace srrg_matchable;
using namespace srrg_matchable_detector;
using namespace srrg_sasha;
using namespace std;

#if QT_VERSION >= 0x050000
typedef qreal qglviewer_real;
#else
typedef float qglviewer_real;
#endif

const int min_age = 20;
srrg_core::Cloud3D cloud;
PlaneDetector* plane_detector;

void generateNoisyVector(srrg_core::Vector6f& v, const float& noise_t, const float& noise_q) {
  float lnx = static_cast <float> (rand()) / (static_cast <float> (RAND_MAX/(noise_t*2)));
  float lny = static_cast <float> (rand()) / (static_cast <float> (RAND_MAX/(noise_t*2)));
  float lnz = static_cast <float> (rand()) / (static_cast <float> (RAND_MAX/(noise_t*2)));
  float lnqx = static_cast <float> (rand()) / (static_cast <float> (RAND_MAX/(noise_q*2)));
  float lnqy = static_cast <float> (rand()) / (static_cast <float> (RAND_MAX/(noise_q*2)));
  float lnqz = static_cast <float> (rand()) / (static_cast <float> (RAND_MAX/(noise_q*2)));
  v << lnx - noise_t, lny - noise_t, lnz - noise_t, lnqx - noise_q, lnqy - noise_q, lnqz - noise_q;
}

class StandardCamera: public qglviewer::Camera {
protected:

  virtual qglviewer_real zFar() const { return 100; }
  virtual qglviewer_real zNear() const { return 0.1; }
};

class Viewer : public QGLViewer {
public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
  Viewer():half(.5,.5,.5){
    StandardCamera * cam =new StandardCamera();
    setCamera(cam);
    cam->setPosition(qglviewer::Vec(5,5,5));
    cam->setViewDirection(qglviewer::Vec(-1,-1,-1));

    // transform
    srrg_core::Vector6f(v);
    //v << 0,0,0,0,0,0;
    v << .3,-.8,.5,.5,-.5,.5;
    //v << .1,.1,.1,.1,.1,.1;
    T=srrg_core::v2tEuler(v);
    iT=T.inverse();
    world_size=2.5;
    point_span=5;
    solver=&iterative_solver;
    reset();
    show_direction_matrix=false;
    solver_constructed=false;
    for(int i=0; i<num_constraint_types; i++)
      num_constraints[i]=0;
    solver->setDamping(.1);


    low_noise = false;
    high_noise = false;
    low_noise_t = .02f;
    low_noise_q = .05f;
    high_noise_t = .1f;
    high_noise_q = .1f;

    cumulative_error = 0.f;
    
    textPen = QPen(Qt::black);
    textFont.setPixelSize(20);

    verbosity = true;
    writer = new std::ofstream("cumulative_error.txt");
  }

  ~Viewer() {
    writer->close();
  }
  
  Scene s_fixed;
  Scene s_moving;
  BaseRegistrationSolver* solver;
  IterativeRegistrationSolver iterative_solver;
  DirectRegistrationSolver direct_solver;
  std::ofstream* writer;
  QFont textFont;
  QPen circlePen;
  QPen textPen;
  
  Eigen::Isometry3f T,iT;
  float world_size;
  bool show_direction_matrix;
  bool solver_constructed;

  bool low_noise;
  bool high_noise;
  float low_noise_t, low_noise_q, high_noise_t, high_noise_q;

  bool verbosity;
  
  float point_span;

  float cumulative_error;
  
  static const char* constraint_types[];

  static const int num_constraint_types=16;
  
  const Eigen::Vector3f half;

  int num_constraints[num_constraint_types];
public :

  virtual void init() {
    QColor white = QColor(Qt::white);
    setBackgroundColor(white);
    glEnable(GL_BLEND);
    glEnable(GL_LIGHTING);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glHint(GL_POLYGON_SMOOTH_HINT, GL_NICEST);
    const GLfloat pos[4] = {0.f, -1.f, -1.f, 1.0f};
    glLightfv(GL_LIGHT1, GL_POSITION, pos);
    glDisable(GL_LIGHT0);
    glEnable(GL_LIGHT1);
    const GLfloat light_ambient[4] = {.5f,.5f,.5f, .5f};
    glLightfv(GL_LIGHT1, GL_AMBIENT, light_ambient);
    const GLfloat light_diffuse[4] = {1.0, 1.0, 1.0, 1.0};
    glLightfv(GL_LIGHT1, GL_DIFFUSE, light_diffuse);
  }

  void printConstraintSummary() {
    using namespace std;
    std::cerr << " 1   |  Point-Point   constraint: " << num_constraints[0] << std::endl;
    std::cerr << " 2   |  Point-Line    constraint: " << num_constraints[1] << std::endl;
    std::cerr << " 3   |  Point-Plane   constraint: " << num_constraints[2] << std::endl;
    std::cerr << " 4   |  Point-Surfel  constraint: " << num_constraints[3] << std::endl;
    std::cerr << " 5   |  Line-Point    constraint: " << num_constraints[4] << std::endl;
    std::cerr << " 6   |  Line-Line     constraint: " << num_constraints[5] << std::endl;
    std::cerr << " 7   |  Line-Plane    constraint: " << num_constraints[6] << std::endl;
    std::cerr << " 8   |  Line-Suefel   constraint: " << num_constraints[7] << std::endl;
    std::cerr << " 9   |  Plane-Point   constraint: " << num_constraints[8] << std::endl;
    std::cerr << " 0   |  Plane-Line    constraint: " << num_constraints[9] << std::endl;
    std::cerr << " Q   |  Plane-Plane   constraint: " << num_constraints[10] << std::endl;
    std::cerr << " W   |  Plane-Surfel  constraint: " << num_constraints[11] << std::endl;
    std::cerr << " E   |  Surfel-Point  constraint: " << num_constraints[12] << std::endl;
    std::cerr << " R   |  Surfel-Line   constraint: " << num_constraints[13] << std::endl;
    std::cerr << " T   |  Surfel-Plane  constraint: " << num_constraints[14] << std::endl;
    std::cerr << " Y   |  Surfel-Surfel constraint: " << num_constraints[15] << std::endl;
  }

  void reset(){
    s_fixed.clear();
    s_moving.clear();
    solver->constraints().clear();
    solver_constructed=false;
    for (int i=0; i<num_constraint_types; i++)
      num_constraints[i]=0;
    if(verbosity)
      printConstraintSummary();
  }


  void addPointToPointConstraint(){
    Eigen::Vector3f p=(Eigen::Vector3f::Random()-half)*world_size;
    MatchablePtr fixed_point= MatchablePtr(new Matchable(Matchable::Point,p)); fixed_point->age() = min_age;
    MatchablePtr moving_point=MatchablePtr(new Matchable(Matchable::Point,p)); moving_point->age() = min_age;

    srrg_core::Vector6f v_noise;
    v_noise.setZero();
    if(low_noise)
      generateNoisyVector(v_noise, low_noise_t, low_noise_q);
    else if(high_noise)
      generateNoisyVector(v_noise, high_noise_t, high_noise_q);
    
    //    if(verbosity)
    std::cerr << "noise: " <<  v_noise.transpose() << std::endl;
    moving_point->transformInPlace(iT * srrg_core::v2t(v_noise));
    s_fixed.push_back(fixed_point);
    s_moving.push_back(moving_point);
    solver->constraints().push_back(BaseRegistrationSolver::Constraint(fixed_point,moving_point));
    num_constraints[0]++;
    solver_constructed=false;
    if(verbosity)    printConstraintSummary();
  }

  void addNoiseToScene(){
    srrg_core::Vector6f v_noise;
    v_noise.setZero();

    if(low_noise)
      generateNoisyVector(v_noise, low_noise_t, low_noise_q);
    else if(high_noise)
      generateNoisyVector(v_noise, high_noise_t, high_noise_q);
    for(MatchablePtr& matchable : s_moving)
      matchable->transformInPlace(srrg_core::v2t(v_noise));
   
  }
  
  void addPointToLineConstraint(){
    Eigen::Vector3f p=(Eigen::Vector3f::Random()-half)*world_size;
    Eigen::Vector3f d=Eigen::Vector3f::Random().normalized();
    MatchablePtr fixed_line = MatchablePtr(new Matchable(Matchable::Line,p));
    fixed_line->age() = min_age;
    fixed_line->setRotation(d);
    MatchablePtr moving_point = MatchablePtr(new Matchable(Matchable::Point, p+d*(point_span*(drand48()-.5))));
    moving_point->age() = min_age;

    Eigen::Matrix3f rotation;
    plane_detector->computePlaneRotationMatrix(rotation,d);
    fixed_line->setRotationMatrix(rotation);

    
    srrg_core::Vector6f v_noise;
    v_noise.setZero();
    if(low_noise)
      generateNoisyVector(v_noise, low_noise_t, low_noise_q);
    else if(high_noise)
      generateNoisyVector(v_noise, high_noise_t, high_noise_q);
    
    if(verbosity)    std::cerr << "noise: " <<  v_noise.transpose() << std::endl;
    moving_point->transformInPlace(iT * srrg_core::v2t(v_noise));

    s_fixed.push_back(fixed_line);
    s_moving.push_back(moving_point);
    solver->constraints().push_back(BaseRegistrationSolver::Constraint(fixed_line,moving_point));
    num_constraints[1]++;
    solver_constructed=false;
    if(verbosity) printConstraintSummary();
  }

  void addPointToPlaneConstraint(){
    Eigen::Vector3f p=(Eigen::Vector3f::Random()-half)*world_size;
    Eigen::Vector3f d=Eigen::Vector3f::Random().normalized();
    MatchablePtr fixed_plane = MatchablePtr(new Matchable(Matchable::Plane,p));
    fixed_plane->setRotation(d);
    fixed_plane->age() = min_age;
    fixed_plane->setCloud(cloud);
    fixed_plane->voxelizeCloud();
    Eigen::Matrix3f rotation;
    plane_detector->computePlaneRotationMatrix(rotation,d);
    fixed_plane->setRotationMatrix(rotation);
    
    MatchablePtr moving_point = MatchablePtr(new Matchable(Matchable::Point,p)); moving_point->age() = min_age;

    srrg_core::Vector6f v_noise;
    v_noise.setZero();
    if(low_noise)
      generateNoisyVector(v_noise, low_noise_t, low_noise_q);
    else if(high_noise)
      generateNoisyVector(v_noise, high_noise_t, high_noise_q);
    
    if(verbosity)    std::cerr << "noise: " <<  v_noise.transpose() << std::endl;
    moving_point->transformInPlace(iT * srrg_core::v2t(v_noise));

    s_fixed.push_back(fixed_plane);
    s_moving.push_back(moving_point);
    solver->constraints().push_back(BaseRegistrationSolver::Constraint(fixed_plane,moving_point));
    num_constraints[2]++;
    solver_constructed=false;
    if(verbosity)    printConstraintSummary();
  }

  void addPointToSurfelConstraint(){
    Eigen::Vector3f p=(Eigen::Vector3f::Random()-half)*world_size;
    Eigen::Vector3f d=Eigen::Vector3f::Random().normalized();
    MatchablePtr fixed_surfel = MatchablePtr(new Matchable(Matchable::Surfel,p));
    fixed_surfel->setRotation(d);
    fixed_surfel->age() = min_age;

    Eigen::Matrix3f rotation;
    plane_detector->computePlaneRotationMatrix(rotation,d);
    fixed_surfel->setRotationMatrix(rotation);
    
    MatchablePtr moving_point = MatchablePtr(new Matchable(Matchable::Point,p)); moving_point->age() = min_age;

    srrg_core::Vector6f v_noise;
    v_noise.setZero();
    if(low_noise)
      generateNoisyVector(v_noise, low_noise_t, low_noise_q);
    else if(high_noise)
      generateNoisyVector(v_noise, high_noise_t, high_noise_q);
    
    if(verbosity)    std::cerr << "noise: " <<  v_noise.transpose() << std::endl;
    moving_point->transformInPlace(iT * srrg_core::v2t(v_noise));

    s_fixed.push_back(fixed_surfel);
    s_moving.push_back(moving_point);
    solver->constraints().push_back(BaseRegistrationSolver::Constraint(fixed_surfel,moving_point));
    num_constraints[3]++;
    solver_constructed=false;
    if(verbosity)    printConstraintSummary();
  }
  
  void addLineToPointConstraint(){
    Eigen::Vector3f p=(Eigen::Vector3f::Random()-half)*world_size;
    Eigen::Vector3f d=Eigen::Vector3f::Random().normalized();
    MatchablePtr fixed_point = MatchablePtr(new Matchable(Matchable::Point,p+d*(point_span*(drand48()-.5))));
    fixed_point->age() = min_age;
    MatchablePtr moving_line =MatchablePtr(new Matchable(Matchable::Line,p));
    moving_line->setRotation(d);
    moving_line->age() = min_age;

    Eigen::Matrix3f rotation;
    plane_detector->computePlaneRotationMatrix(rotation,d);
    moving_line->setRotationMatrix(rotation);

    
    srrg_core::Vector6f v_noise;
    v_noise.setZero();
    if(low_noise)
      generateNoisyVector(v_noise, low_noise_t, low_noise_q);
    else if(high_noise)
      generateNoisyVector(v_noise, high_noise_t, high_noise_q);
    
    if(verbosity)    std::cerr << "noise: " <<  v_noise.transpose() << std::endl;
    
    moving_line->transformInPlace(iT * srrg_core::v2t(v_noise));

    s_fixed.push_back(fixed_point);
    s_moving.push_back(moving_line);
    solver->constraints().push_back(BaseRegistrationSolver::Constraint(fixed_point,moving_line));
    num_constraints[4]++;
    solver_constructed=false;
    if(verbosity)    printConstraintSummary();
  }

  void addLineToLineConstraint(){
    using namespace std;
    Eigen::Vector3f p=(Eigen::Vector3f::Random()-half)*world_size;
    Eigen::Vector3f d=Eigen::Vector3f::Random().normalized();
    MatchablePtr fixed_line=MatchablePtr(new Matchable(Matchable::Line, p));
    fixed_line->age() = min_age;
    fixed_line->setRotation(d);
    MatchablePtr moving_line=MatchablePtr(new Matchable(Matchable::Line, 
                                                        p+d*(point_span*(drand48()-.5))));
    moving_line->setRotation(d);
    moving_line->age() = min_age;


    Eigen::Matrix3f rotation;
    plane_detector->computePlaneRotationMatrix(rotation,d);
    moving_line->setRotationMatrix(rotation);
    fixed_line->setRotationMatrix(rotation);
    
    srrg_core::Vector6f v_noise;
    v_noise.setZero();
    if(low_noise)
      generateNoisyVector(v_noise, low_noise_t, low_noise_q);
    else if(high_noise)
      generateNoisyVector(v_noise, high_noise_t, high_noise_q);
    
    if(verbosity)    std::cerr << "noise: " <<  v_noise.transpose() << std::endl;
    moving_line->transformInPlace(iT * srrg_core::v2t(v_noise));

    s_fixed.push_back(fixed_line);
    s_moving.push_back(moving_line);
    solver->constraints().push_back(BaseRegistrationSolver::Constraint(fixed_line,moving_line));
    num_constraints[5]++;
    solver_constructed=false;
    if(verbosity)    printConstraintSummary();
  }

  void addLineToPlaneConstraint(){
    Eigen::Vector3f p=(Eigen::Vector3f::Random()-half)*world_size;
    Eigen::Vector3f d=Eigen::Vector3f::Random().normalized();
    MatchablePtr moving_line=MatchablePtr(new Matchable(Matchable::Line,p));
    moving_line->setRotation(d);
    moving_line->age() = min_age;

    Eigen::Matrix3f rotation;
    plane_detector->computePlaneRotationMatrix(rotation,d);
    moving_line->setRotationMatrix(rotation);

    
    Eigen::Vector3f dn=d.cross(p).normalized();
    MatchablePtr fixed_plane=MatchablePtr(new Matchable(Matchable::Plane,p));
    fixed_plane->setRotation(dn);
    fixed_plane->age() = min_age;
    fixed_plane->setCloud(cloud);
    plane_detector->computePlaneRotationMatrix(rotation,dn);
    fixed_plane->setRotationMatrix(rotation);
    fixed_plane->voxelizeCloud();
    
    srrg_core::Vector6f v_noise;
    v_noise.setZero();
    if(low_noise)
      generateNoisyVector(v_noise, low_noise_t, low_noise_q);
    else if(high_noise)
      generateNoisyVector(v_noise, high_noise_t, high_noise_q);
    
    if(verbosity)    std::cerr << "noise: " <<  v_noise.transpose() << std::endl;
    moving_line->transformInPlace(iT * srrg_core::v2t(v_noise));

    s_fixed.push_back(fixed_plane);
    s_moving.push_back(moving_line);
    solver->constraints().push_back(BaseRegistrationSolver::Constraint(fixed_plane,moving_line));
    num_constraints[6]++;
    solver_constructed=false;
    if(verbosity)    printConstraintSummary();
  }

  void addLineToSurfelConstraint(){
    Eigen::Vector3f p=(Eigen::Vector3f::Random()-half)*world_size;
    Eigen::Vector3f d=Eigen::Vector3f::Random().normalized();
    MatchablePtr moving_line=MatchablePtr(new Matchable(Matchable::Line,p));
    moving_line->setRotation(d);
    moving_line->age() = min_age;

    Eigen::Matrix3f rotation;
    plane_detector->computePlaneRotationMatrix(rotation,d);
    moving_line->setRotationMatrix(rotation);

    
    Eigen::Vector3f dn=d.cross(p).normalized();
    MatchablePtr fixed_surfel=MatchablePtr(new Matchable(Matchable::Surfel,p));
    fixed_surfel->setRotation(dn);
    fixed_surfel->age() = min_age;

    plane_detector->computePlaneRotationMatrix(rotation,dn);
    fixed_surfel->setRotationMatrix(rotation);
    
    srrg_core::Vector6f v_noise;
    v_noise.setZero();
    if(low_noise)
      generateNoisyVector(v_noise, low_noise_t, low_noise_q);
    else if(high_noise)
      generateNoisyVector(v_noise, high_noise_t, high_noise_q);
    
    if(verbosity)    std::cerr << "noise: " <<  v_noise.transpose() << std::endl;
    moving_line->transformInPlace(iT * srrg_core::v2t(v_noise));

    s_fixed.push_back(fixed_surfel);
    s_moving.push_back(moving_line);
    solver->constraints().push_back(BaseRegistrationSolver::Constraint(fixed_surfel,moving_line));
    num_constraints[7]++;
    solver_constructed=false;
    if(verbosity)    printConstraintSummary();
  }

  
  void addPlaneToPointConstraint(){
    Eigen::Vector3f p=(Eigen::Vector3f::Random()-half)*world_size;
    Eigen::Vector3f d=Eigen::Vector3f::Random().normalized();
    MatchablePtr moving_plane = MatchablePtr(new Matchable(Matchable::Plane,p));
    moving_plane->setRotation(d);
    moving_plane->age() = min_age;
    moving_plane->setCloud(cloud);
    Eigen::Matrix3f rotation;
    plane_detector->computePlaneRotationMatrix(rotation,d);
    moving_plane->setRotationMatrix(rotation);
    MatchablePtr fixed_point = MatchablePtr(new Matchable(Matchable::Point,p)); fixed_point->age() = min_age;

    srrg_core::Vector6f v_noise;
    v_noise.setZero();
    if(low_noise)
      generateNoisyVector(v_noise, low_noise_t, low_noise_q);
    else if(high_noise)
      generateNoisyVector(v_noise, high_noise_t, high_noise_q);    
    if(verbosity)    std::cerr << "noise: " <<  v_noise.transpose() << std::endl;    
    moving_plane->transformInPlace(iT * srrg_core::v2t(v_noise));

    moving_plane->voxelizeCloud();
    
    s_fixed.push_back(fixed_point);
    s_moving.push_back(moving_plane);
    solver->constraints().push_back(BaseRegistrationSolver::Constraint(fixed_point,moving_plane));
    num_constraints[8]++;
    solver_constructed=false;
    if(verbosity)    printConstraintSummary();
  }

  void addPlaneToLineConstraint(){
    Eigen::Vector3f p=(Eigen::Vector3f::Random()-half)*world_size;
    Eigen::Vector3f d=Eigen::Vector3f::Random().normalized();
    MatchablePtr fixed_line=MatchablePtr(new Matchable(Matchable::Line,p));
    fixed_line->setRotation(d);
    fixed_line->age() = min_age;

    Eigen::Matrix3f rotation;
    plane_detector->computePlaneRotationMatrix(rotation,d);
    fixed_line->setRotationMatrix(rotation);
    
    Eigen::Vector3f dn=d.cross(p).normalized();
    MatchablePtr moving_plane=MatchablePtr(new Matchable(Matchable::Plane,p));
    moving_plane->setRotation(dn);
    moving_plane->age() = min_age;
    moving_plane->setCloud(cloud);
    plane_detector->computePlaneRotationMatrix(rotation,dn);
    moving_plane->setRotationMatrix(rotation);
    moving_plane->voxelizeCloud();
    
    srrg_core::Vector6f v_noise;
    v_noise.setZero();
    if(low_noise)
      generateNoisyVector(v_noise, low_noise_t, low_noise_q);
    else if(high_noise)
      generateNoisyVector(v_noise, high_noise_t, high_noise_q);
    
    if(verbosity)    std::cerr << "noise: " <<  v_noise.transpose() << std::endl;
    moving_plane->transformInPlace(iT * srrg_core::v2t(v_noise));
    
    s_fixed.push_back(fixed_line);
    s_moving.push_back(moving_plane);
    solver->constraints().push_back(BaseRegistrationSolver::Constraint(fixed_line,moving_plane));
    num_constraints[9]++;
    solver_constructed=false;
    if(verbosity)    printConstraintSummary();
  }


  void addPlaneToPlaneConstraint(){
    Eigen::Vector3f p=(Eigen::Vector3f::Random()-half)*world_size;
    Eigen::Vector3f d=Eigen::Vector3f::Random().normalized();
    MatchablePtr fixed_plane=MatchablePtr(new Matchable(Matchable::Plane,p));
    fixed_plane->setRotation(d);
    fixed_plane->age() = min_age;
    fixed_plane->setCloud(cloud);
    Eigen::Matrix3f rotation;
    plane_detector->computePlaneRotationMatrix(rotation,d);
    fixed_plane->setRotationMatrix(rotation);
    fixed_plane->voxelizeCloud();

    MatchablePtr moving_plane=MatchablePtr(new Matchable(Matchable::Plane,p));
    moving_plane->setRotation(d);
    moving_plane->age() = min_age;
    moving_plane->setCloud(cloud);
    plane_detector->computePlaneRotationMatrix(rotation,d);
    moving_plane->setRotationMatrix(rotation);
    moving_plane->voxelizeCloud();

    srrg_core::Vector6f v_noise;
    v_noise.setZero();
    if(low_noise)
      generateNoisyVector(v_noise, low_noise_t, low_noise_q);
    else if(high_noise)
      generateNoisyVector(v_noise, high_noise_t, high_noise_q);
    
    if(verbosity)    std::cerr << "noise: " <<  v_noise.transpose() << std::endl;
    moving_plane->transformInPlace(iT * srrg_core::v2t(v_noise));

    s_fixed.push_back(fixed_plane);
    s_moving.push_back(moving_plane);
    solver->constraints().push_back(BaseRegistrationSolver::Constraint(fixed_plane,moving_plane));
    num_constraints[10]++;
    solver_constructed=false;
    if(verbosity)    printConstraintSummary();
  }

  void addPlaneToSurfelConstraint(){
    Eigen::Vector3f p=(Eigen::Vector3f::Random()-half)*world_size;
    Eigen::Vector3f d=Eigen::Vector3f::Random().normalized();
    MatchablePtr fixed_surfel=MatchablePtr(new Matchable(Matchable::Surfel,p));
    fixed_surfel->setRotation(d);
    fixed_surfel->age() = min_age;
    fixed_surfel->setCloud(cloud);
    Eigen::Matrix3f rotation;
    plane_detector->computePlaneRotationMatrix(rotation,d);
    fixed_surfel->setRotationMatrix(rotation);

    
    MatchablePtr moving_plane=MatchablePtr(new Matchable(Matchable::Plane,p));
    moving_plane->setRotation(d);
    moving_plane->age() = min_age;
    moving_plane->setCloud(cloud);
    plane_detector->computePlaneRotationMatrix(rotation,d);
    moving_plane->setRotationMatrix(rotation);
    moving_plane->voxelizeCloud();

    srrg_core::Vector6f v_noise;
    v_noise.setZero();
    if(low_noise)
      generateNoisyVector(v_noise, low_noise_t, low_noise_q);
    else if(high_noise)
      generateNoisyVector(v_noise, high_noise_t, high_noise_q);
    
    if(verbosity)    std::cerr << "noise: " <<  v_noise.transpose() << std::endl;
    moving_plane->transformInPlace(iT * srrg_core::v2t(v_noise));

    s_fixed.push_back(fixed_surfel);
    s_moving.push_back(moving_plane);
    solver->constraints().push_back(BaseRegistrationSolver::Constraint(fixed_surfel,moving_plane));
    num_constraints[11]++;
    solver_constructed=false;
    if(verbosity)    printConstraintSummary();
  }
  
  void addSurfelToPointConstraint(){
    Eigen::Vector3f p=(Eigen::Vector3f::Random()-half)*world_size;
    Eigen::Vector3f d=Eigen::Vector3f::Random().normalized();        
    MatchablePtr fixed_point= MatchablePtr(new Matchable(Matchable::Point,p)); fixed_point->age() = min_age;
    MatchablePtr moving_surfel=MatchablePtr(new Matchable(Matchable::Surfel,p));
    moving_surfel->setRotation(d);
    moving_surfel->age() = min_age;

    Eigen::Matrix3f rotation;
    plane_detector->computePlaneRotationMatrix(rotation,d);    
    moving_surfel->setRotationMatrix(rotation);

    
    srrg_core::Vector6f v_noise;
    v_noise.setZero();
    if(low_noise)
      generateNoisyVector(v_noise, low_noise_t, low_noise_q);
    else if(high_noise)
      generateNoisyVector(v_noise, high_noise_t, high_noise_q);
    
    if(verbosity)    std::cerr << "noise: " <<  v_noise.transpose() << std::endl;
    moving_surfel->transformInPlace(iT * srrg_core::v2t(v_noise));
    s_fixed.push_back(fixed_point);
    s_moving.push_back(moving_surfel);
    solver->constraints().push_back(BaseRegistrationSolver::Constraint(fixed_point,moving_surfel));
    num_constraints[12]++;
    solver_constructed=false;
    if(verbosity)    printConstraintSummary();
  }
  
  void addSurfelToLineConstraint(){
    Eigen::Vector3f p=(Eigen::Vector3f::Random()-half)*world_size;
    Eigen::Vector3f d=Eigen::Vector3f::Random().normalized();
    MatchablePtr fixed_line = MatchablePtr(new Matchable(Matchable::Line,p));
    fixed_line->setRotation(d);
    fixed_line->age() = min_age;

    Eigen::Matrix3f rotation;
    plane_detector->computePlaneRotationMatrix(rotation, d);
    fixed_line->setRotationMatrix(rotation);
    Eigen::Vector3f dn=d.cross(p).normalized();

    MatchablePtr moving_surfel = MatchablePtr(new Matchable(Matchable::Surfel, p+d*(point_span*(drand48()-.5))));
    moving_surfel->setRotation(dn);
    moving_surfel->age() = min_age;

    plane_detector->computePlaneRotationMatrix(rotation,dn);
    moving_surfel->setRotationMatrix(rotation);

    
    srrg_core::Vector6f v_noise;
    v_noise.setZero();
    if(low_noise)
      generateNoisyVector(v_noise, low_noise_t, low_noise_q);
    else if(high_noise)
      generateNoisyVector(v_noise, high_noise_t, high_noise_q);
    
    if(verbosity)    std::cerr << "noise: " <<  v_noise.transpose() << std::endl;
    moving_surfel->transformInPlace(iT * srrg_core::v2t(v_noise));

    s_fixed.push_back(fixed_line);
    s_moving.push_back(moving_surfel);
    solver->constraints().push_back(BaseRegistrationSolver::Constraint(fixed_line,moving_surfel));
    num_constraints[13]++;
    solver_constructed=false;
    if(verbosity)    printConstraintSummary();
  }

  void addSurfelToPlaneConstraint(){
    Eigen::Vector3f p=(Eigen::Vector3f::Random()-half)*world_size;
    Eigen::Vector3f d=Eigen::Vector3f::Random().normalized();
    MatchablePtr fixed_plane = MatchablePtr(new Matchable(Matchable::Plane,p));
    fixed_plane->setRotation(d);
    fixed_plane->age() = min_age;
    fixed_plane->setCloud(cloud);
    fixed_plane->voxelizeCloud();
    Eigen::Matrix3f rotation;
    plane_detector->computePlaneRotationMatrix(rotation,d);
    fixed_plane->setRotationMatrix(rotation);
    
    MatchablePtr moving_surfel = MatchablePtr(new Matchable(Matchable::Surfel,p));
    moving_surfel->setRotation(d);
    moving_surfel->age() = min_age;

    plane_detector->computePlaneRotationMatrix(rotation,d);
    moving_surfel->setRotationMatrix(rotation);

    
    srrg_core::Vector6f v_noise;
    v_noise.setZero();
    if(low_noise)
      generateNoisyVector(v_noise, low_noise_t, low_noise_q);
    else if(high_noise)
      generateNoisyVector(v_noise, high_noise_t, high_noise_q);
    
    if(verbosity)    std::cerr << "noise: " <<  v_noise.transpose() << std::endl;
    moving_surfel->transformInPlace(iT * srrg_core::v2t(v_noise));
    
    s_fixed.push_back(fixed_plane);
    s_moving.push_back(moving_surfel);
    solver->constraints().push_back(BaseRegistrationSolver::Constraint(fixed_plane,moving_surfel));
    num_constraints[14]++;
    solver_constructed=false;
    if(verbosity)    printConstraintSummary();
  }


  void addSurfelToSurfelConstraint(){
    Eigen::Vector3f p=(Eigen::Vector3f::Random()-half)*world_size;
    Eigen::Vector3f d=Eigen::Vector3f::Random().normalized();        
    MatchablePtr fixed_surfel= MatchablePtr(new Matchable(Matchable::Surfel,p));
    fixed_surfel->setRotation(d);
    fixed_surfel->age() = min_age;
    MatchablePtr moving_surfel=MatchablePtr(new Matchable(Matchable::Surfel,p));
    moving_surfel->setRotation(d);
    moving_surfel->age() = min_age;

    Eigen::Matrix3f rotation;
    plane_detector->computePlaneRotationMatrix(rotation,d);
    fixed_surfel->setRotationMatrix(rotation);
    moving_surfel->setRotationMatrix(rotation);

    srrg_core::Vector6f v_noise;
    v_noise.setZero();
    if(low_noise)
      generateNoisyVector(v_noise, low_noise_t, low_noise_q);
    else if(high_noise)
      generateNoisyVector(v_noise, high_noise_t, high_noise_q);
    
    if(verbosity)    std::cerr << "noise: " <<  v_noise.transpose() << std::endl;
    moving_surfel->transformInPlace(iT * srrg_core::v2t(v_noise));
    
    s_fixed.push_back(fixed_surfel);
    s_moving.push_back(moving_surfel);
    solver->constraints().push_back(BaseRegistrationSolver::Constraint(fixed_surfel,moving_surfel));
    num_constraints[15]++;
    solver_constructed=false;
    if(verbosity)    printConstraintSummary();
  }

  

  virtual void draw() {
    s_fixed.draw(show_direction_matrix);
    Eigen::Isometry3f current_transform=T;
    if (solver_constructed)
      current_transform=solver->transform();
    glPushMatrix();
    srrg_gl_helpers::glMultMatrix(current_transform);
    s_moving.draw(show_direction_matrix);
    glPopMatrix();
    drawConstraints();
  }

  void oneRound(bool ideal_guess=false) {
    using namespace std;
    if (solver->constraints().empty()) {
      cerr << "no constraints in problem" << endl;
      return;
    }
    if (!solver_constructed) {
    if(verbosity)      cerr << "initializing problem" << endl;
      solver->init(T);
    if(verbosity)      cerr << "solver initialized" << endl;
      solver_constructed=true;
    }
    solver->oneRound();
    int constraint_num=0;
    cumulative_error = 0.f;

        if(verbosity)cerr << "****************************************************" << endl;
    for (const BaseRegistrationSolver::Constraint& constraint: solver->constraints()){
    if(verbosity)      cerr << "constraint_num: " << constraint_num;
    if(verbosity)      cerr << " ep: " << constraint.error_p;
    if(verbosity)      cerr << " ed: " << constraint.error_d;
    if(verbosity)      cerr << " eo: " << constraint.error_o;
    if(verbosity)      cerr << " ex: " << constraint.error_x;
    if(verbosity)      cerr << endl;
      constraint_num++;
      cumulative_error += (constraint.error_p*constraint.error_p + constraint.error_d*constraint.error_d + constraint.error_o*constraint.error_o + constraint.error_x*constraint.error_x);
    }
    if(verbosity)    cerr << "cumulative squared error: " << cumulative_error << std::endl;
    if(verbosity)    cerr << "w" << std::endl;
    *writer << cumulative_error << ", ";
      
    if(verbosity)    cerr << std::endl;
    if(verbosity)    cerr << "delta transform: " << endl;
    if(verbosity)    cerr << iT*solver->transform().matrix() << endl;
    if(verbosity)    cerr << "****************************************************" << endl;

  }

  void drawConstraints(){
    Eigen::Isometry3f current_transform=T;
    if (solver_constructed)
      current_transform=solver->transform();
    glPushAttrib(GL_COLOR|GL_LINE_WIDTH);
    glLineWidth(2);
    glColor4f(0.5,0.5,0.5,1);
    glBegin(GL_LINES);
    for (const BaseRegistrationSolver::Constraint c: solver->constraints()){
      const MatchablePtr fixed=c.fixed;
      const MatchablePtr moving=c.moving;
      glVertex3f(fixed->point().x(),
                 fixed->point().y(),
                 fixed->point().z());
      const Eigen::Vector3f moving_point=current_transform*moving->point();
      glVertex3f(moving_point.x(),
                 moving_point.y(),
                 moving_point.z());
    }
    glEnd();
    glPopAttrib();
  }

  void switchNoise() {
    if(verbosity)    std::cerr << "[Key N]: Switched to ";
    if(!low_noise && !high_noise){
      low_noise = true;
      std::cerr << "LOW noise" << std::endl;
      return;
    }else if(low_noise){
      low_noise = false;
      high_noise = true;
      std::cerr << "HIGH noise" << std::endl;
      return;
    }else{
      high_noise = false;
      std::cerr << "NO noise" << std::endl;
      return;
    }           
  }

  void initProblem() {
    solver->setTransform(Eigen::Isometry3f::Identity());
    if (solver->constraints().empty()) {
      cerr << "no constraints in problem" << endl;
      return;
    }
    if (!solver_constructed) {
      cerr << "initializing problem" << endl;
      solver->init();
      cerr << "problem initialized" << endl;
      solver_constructed=true;
    }
  }

  void switchSolver() {
    BaseRegistrationSolver* previous_solver=solver;
    if (solver==&direct_solver){
      solver=&iterative_solver;
      cerr << "iterative solver selected" << endl;
    } else {
      solver=&direct_solver;
      cerr << "direct solver selected" << endl;
    }
    solver->init(previous_solver->transform());
    solver->constraints()=previous_solver->constraints();
    if (!solver_constructed) {
    if(verbosity)      cerr << "initializing problem" << endl;
      solver->init();
    if(verbosity)      cerr << "problem initialized" << endl;
      solver_constructed=true;
    }      
  }
  
  
  virtual void  keyPressEvent(QKeyEvent *event){
    float loc_cumulative_error = 0.f;
    switch(event->key()){
    case Qt::Key_1:
      addPointToPointConstraint();
      break;
    case Qt::Key_2:
      addPointToLineConstraint();
      break;
    case Qt::Key_3:
      addPointToPlaneConstraint();
      break;
    case Qt::Key_4:
      addPointToSurfelConstraint();
      break;
    case Qt::Key_5:
      addLineToPointConstraint();
      break;
    case Qt::Key_6:
      addLineToLineConstraint();
      break;
    case Qt::Key_7:
      addLineToPlaneConstraint();
      break;
    case Qt::Key_8:
      addLineToSurfelConstraint();
      break;
    case Qt::Key_9:
      addPlaneToPointConstraint();
      break;
    case Qt::Key_0:
      addPlaneToLineConstraint();
      break;
    case Qt::Key_Q:
      addPlaneToPlaneConstraint();
      break;
    case Qt::Key_W:
      addPlaneToSurfelConstraint();
      break;
    case Qt::Key_E:
      addSurfelToPointConstraint();
      break;
    case Qt::Key_R:
      addSurfelToLineConstraint();
      break;
    case Qt::Key_T:
      addSurfelToPlaneConstraint();
      break;
    case Qt::Key_Y:
      addSurfelToSurfelConstraint();
      break;
           
    case Qt::Key_Delete:
      reset();
      break;

    case Qt::Key_U:
      for(const BaseRegistrationSolver::Constraint& constraint : solver->constraints()) {
        loc_cumulative_error += (constraint.error_p*constraint.error_p + constraint.error_d*constraint.error_d + constraint.error_o*constraint.error_o + constraint.error_x*constraint.error_x);    
      }
      std::cerr << "[IRRUMATIO] cum error: " << loc_cumulative_error << std::endl;     
      break;
      
    case Qt::Key_X:
      initProblem();
      break;
      
    case Qt::Key_S:
      switchSolver();
      break;

    case Qt::Key_D:
      show_direction_matrix=!show_direction_matrix;
      break;
      
    case Qt::Key_Space:
      std::cerr << "Space pressed" << std::endl;
      oneRound();
      break;
      
    case Qt::Key_N:
      switchNoise();
      break;
      
    case Qt::Key_H:
      std::cerr << FGRN("[SyntheticViewer] Help: ") << std::endl;
      std::cerr << FGRN(" KEY |   EFFECT                  STATUS") << std::endl;
      printConstraintSummary();
      std::cerr << "______________________________" << std::endl;
      std::cerr << "DEL  |  reset                 " << std::endl;
      std::cerr << " S   |  solver{iterative/direct}" << std::endl;
      std::cerr << " X   |  init                  " << std::endl;
      std::cerr << " D   |  show direction matrix " << std::endl;
      std::cerr << "SPACE|  one round             " << std::endl;
      std::cerr << " N   |  switch noise          " << std::endl;      
      std::cerr << " U   |  print cumulative error" << std::endl;
      std::cerr << " H   |  this help             " << std::endl;
      break;
      
    default: QGLViewer::keyPressEvent(event);
    }
    updateGL();
  }



  void paintEvent(QPaintEvent *event) {
    Q_UNUSED(event)
      QPainter painter;
    painter.begin(this);
    painter.setRenderHint(QPainter::Antialiasing);
    painter.setPen(textPen);
    painter.setFont(textFont);
    
    // Save current OpenGL state
    glPushAttrib(GL_ALL_ATTRIB_BITS);
    glMatrixMode(GL_PROJECTION);
    glPushMatrix();
    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    
    // Reset OpenGL parameters
    glEnable(GL_LIGHTING);
    const GLfloat pos[4] = {0.f, -1.f, -1.f, 1.0f};
    glLightfv(GL_LIGHT1, GL_POSITION, pos);
    glDisable(GL_LIGHT0);
    glEnable(GL_LIGHT1);
    
    const GLfloat light_ambient[4] = {.5f,.5f,.5f, .5f};
    glLightfv(GL_LIGHT1, GL_AMBIENT, light_ambient);
    
    const GLfloat light_diffuse[4] = {1.0, 1.0, 1.0, 1.0};
    glLightfv(GL_LIGHT1, GL_DIFFUSE, light_diffuse);
    
    glShadeModel(GL_SMOOTH);
    glEnable(GL_DEPTH_TEST);
    //    glEnable(GL_CULL_FACE);
    glDepthFunc(GL_LEQUAL);

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_LIGHTING);
    glHint(GL_POLYGON_SMOOTH_HINT, GL_NICEST);

    // Classical 3D drawing, usually performed by paintGL().
    preDraw();
    draw();
    postDraw();
    // Restore OpenGL state
    glMatrixMode(GL_MODELVIEW);
    glPopMatrix();
    glMatrixMode(GL_PROJECTION);
    glPopMatrix();
    glPopAttrib();

    painter.end();
  }


};

const char* Viewer::constraint_types[]={"point-point", "point-line",  "point-plane", "point-surfel",
                                        "line-point",  "line-line",   "line-plane", "line-surfel",
                                        "plane-point", "plane-line",  "plane-plane", "plane-surfel",
                                        "surfel-point", "surfel-line", "surfel-plane", "surfel-surfel"};

// int main(int argc, char** argv) {
//   QApplication application(argc, argv);
//   srand(time(NULL));  
//   Scene* s_fixed=new Scene();
//   plane_detector = new PlaneDetector();  
//   float step = 0.01;
//   int dim = 1000;
//   for(int j=0; j < dim; ++j)
//     for(int i=0; i < dim; ++i){
//       cloud.push_back(srrg_core::RichPoint3D(Eigen::Vector3f(-5+i*step,-5.f+j*step,0),Eigen::Vector3f(0,0,1),1.0f));
//     }  
//   Viewer viewer;
//   viewer.show();
//   return application.exec();
// }

int main(int argc, char** argv) {
  QApplication application(argc, argv);
  Viewer porco_dio;
  porco_dio.verbosity = false;
  std::ofstream file("results.txt");
  
  const size_t constraint_num = 100;  
  for(size_t constraint = 0; constraint < 16; ++constraint) {
    std::cerr << "Generating Experiment " << Viewer::constraint_types[constraint] << std::endl;
    // Generating constraints to populate Porco Dio
    for(size_t i = 0; i < constraint_num; ++i) {
      switch(constraint) {
      case 0:
        porco_dio.addPointToPointConstraint();
        break;
      case 1:
        porco_dio.addPointToLineConstraint();
        break;
      case 2:
        porco_dio.addPointToPlaneConstraint();
        break;
      case 3:
        porco_dio.addPointToSurfelConstraint();
        break;
      case 4:
        porco_dio.addLineToPointConstraint();
        break;
      case 5:
        porco_dio.addLineToLineConstraint();
        break;
      case 6:
        porco_dio.addLineToPlaneConstraint();
        break;
      case 7:
        porco_dio.addLineToSurfelConstraint();
        break;
      case 8:
        porco_dio.addPlaneToPointConstraint();
        break;
      case 9:
        porco_dio.addPlaneToLineConstraint();
        break;
      case 10:
        porco_dio.addPlaneToPlaneConstraint();
        break;
      case 11:
        porco_dio.addPlaneToSurfelConstraint();
        break;
      case 12:
        porco_dio.addSurfelToPointConstraint();
        break;
      case 13:
        porco_dio.addSurfelToLineConstraint();
        break;
      case 14:
        porco_dio.addSurfelToPlaneConstraint();
        break;
      case 15:
        porco_dio.addSurfelToSurfelConstraint();
        break;        
      } // switch
    } // for constraints_num

    // for noise levels
    for(size_t noise = 0; noise < 3; ++noise) {
    // for solvers      
      for(size_t solver = 0; solver < 2; ++solver) {        
        // init problem
        porco_dio.initProblem();
        // iterations
        for(size_t iter = 0; iter < 10; ++iter) {
          porco_dio.oneRound();
          std::cerr << "iter: " << iter << " " << porco_dio.cumulative_error << std::endl;
          file << porco_dio.cumulative_error << " ";
          if(solver == 1) //save only one iter for DIRECT solver
            break;
        }
        porco_dio.switchSolver();
      } //solvers
      porco_dio.switchNoise();
      porco_dio.addNoiseToScene();
    } //noise
    porco_dio.reset();
    file << std::endl;
  } // for constraint
  file.close();
  return 0;
}
