#include <srrg_matchable/matchable.h>
#include <srrg_matchable_detector/matchable_detector.h>

#include <iostream>
#include <iomanip>
#include <string>
#include "opencv2/core/core.hpp"
#include "opencv2/core/utility.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui/highgui.hpp"

#include "srrg_system_utils/system_utils.h"
#include "srrg_messages/message_reader.h"
#include "srrg_messages/pinhole_image_message.h"
#include "srrg_messages/sensor_message_sorter.h"
#include "srrg_messages/message_timestamp_synchronizer.h"
#include <srrg_image_utils/depth_utils.h>

#include <srrg_sasha/shapes_tracker.h>
#include <srrg_sasha_viewers/tracker_viewer.h>

#include <srrg_sasha/evaluate_rpe.hpp>

#include <yaml-cpp/yaml.h>

using namespace std;
using namespace cv;
using namespace srrg_core;
using namespace srrg_matchable;
using namespace srrg_matchable_detector;
using namespace srrg_sasha;

using namespace srrg_rpe;

// bdc, for evaluation in TUM format only
typedef Eigen::Matrix<float, 7, 1> Vector7f;
Vector7f t2vFull(const Eigen::Isometry3f& iso){
  Vector7f v;
  v.head<3>() = iso.translation();
  Eigen::Quaternionf q(iso.linear());
  v(3) = q.x();
  v(4) = q.y();
  v(5) = q.z();
  v(6) = q.w();
  return v;
}

void initDetectorFromYAML(const std::string& yaml_file,
                          MatchableDetector& detector);
void printTimeStats(const ShapesTracker::TimeStats& tracker_time_stats,
                    const ShapesAligner::TimeStats& aligner_time_stats,
                    const srrg_matchable_detector::MatchableDetector::TimeStats& detector_time_stats);


PinholeImageMessage i;

const char* banner[] = {
  "track_and_evaluate: sasha gray in our hearts",
  "",
  "usage: track_and_evaluate <options> <dump_file>",
  "-t       <string>           depth image topic name, as in the txtio file, default /camera/depth/image_raw",
  "-rgbt    <string>           rgb image topic name, as in the txtio file, default /camera/rgb/image_raw",
  "-npt     <flag>             do NOT use POINTS",
  "-nl      <flag>             do NOT use LINES",
  "-npl     <flag>             do NOT use PLANES",
  "-ns      <flag>             do NOT use SURFELS",
  "-use_w   <flag>             use support weight",
  "-cc      <flag>             use cross constraint (with BF{slow})",
  "-direct  <flag>             use direct solver",
  "-skip    <int>              frame skip, default 0",
  "-config  <string>           yaml config file",
  "-v       <flag>             verbose mode",
  "-no_gui  <flag>             no GUI mode",
  "-show_images     <flag>     show images",
  "-show_matchables <flag>     show detected matchables",
  "-stats           <flag>     print time stats",
  "-gt_file         <string>   ground truth file in TUM format to evaluate",
  "-o               <string>   output_file in TUM format, default out.txt",
  "-mc              <string>   output file to count matchables, default empty",
  0
};



int main(int argc, char** argv){

  if (argc < 2 || !strcmp(argv[1], "-h")) {
    printBanner(banner);
    return 0;
  }

  std::string filename = "";
  std::string gt_file = "";
  std::string depth_topic = "/camera/depth/image_raw";
  std::string rgb_topic = "/camera/rgb/image_raw";
  std::string output = "out.txt";
  std::string matchable_count = "";
  std::string yaml_config = "";
  
  MessageReader reader;

  bool use_points  = true;
  bool use_lines   = true;
  bool use_planes  = true;
  bool use_surfels = true;
  bool use_gui     = true;
  bool use_cc      = false;
  
  bool verbose = false;
  bool time_stats = false;
  
  bool use_support_weight = false;
  
  bool use_direct_solver = false;
  int skip_frames = 0;

  bool show_images = false;
  bool show_matchables = false;

  std::vector<string> depth_plus_rgb_topic;
  
  int c = 1;
  while (c < argc) {
    if (!strcmp(argv[c], "-h")) {
      printBanner(banner);
      return 1;
    } else if (!strcmp(argv[c], "-t")) {
      c++;
      depth_topic = argv[c];
      depth_plus_rgb_topic.push_back(depth_topic);
    } else if (!strcmp(argv[c], "-rgbt")) {
      c++;
      rgb_topic = argv[c];
      depth_plus_rgb_topic.push_back(rgb_topic);
    } else if (!strcmp(argv[c], "-npt")) {
      use_points = false;
    } else if (!strcmp(argv[c], "-nl")) {
      use_lines = false;
    } else if (!strcmp(argv[c], "-npl")) {
      use_planes = false;
    } else if (!strcmp(argv[c], "-ns")) {
      use_surfels = false;
    } else if (!strcmp(argv[c], "-use_w")) {
      use_support_weight = true;
    } else if (!strcmp(argv[c], "-cc")) {
      use_cc = true;
    } else if (!strcmp(argv[c], "-no_gui")) {
      use_gui = false;      
    } else if (!strcmp(argv[c], "-direct")) {
      use_direct_solver = true;
    } else if (!strcmp(argv[c], "-v")) {
      verbose = true;      
    } else if (!strcmp(argv[c], "-stats")) {
      time_stats = true;            
    } else if (!strcmp(argv[c], "-config")) {
      c++;
      yaml_config = argv[c];
    } else if (!strcmp(argv[c], "-skip")) {
      c++;
      skip_frames = std::atoi(argv[c]);
      } else if (!strcmp(argv[c], "-show_images")) {
        show_images = true;
      } else if (!strcmp(argv[c], "-show_matchables")) {
        show_matchables = true;
      } else if (!strcmp(argv[c], "-o")) {
        c++;
        output = argv[c];
      }else if (!strcmp(argv[c], "-mc")) {
        c++;
        matchable_count = argv[c];
      } else if (!strcmp(argv[c], "-gt_file")) {
        c++;
        gt_file = argv[c];
      } else {
        filename = argv[c];
      }
    c++;
  }

  std::cerr << "depth_topic        : " << depth_topic << std::endl;
  std::cerr << "rgb_topic          : " << rgb_topic << std::endl;
  std::cerr << "filename           : " << filename << std::endl;
  std::cerr << "output             : " << output << std::endl;
  std::cerr << "matchable_count    : " << matchable_count << std::endl;
  std::cerr << "use points         : " << use_points << std::endl;
  std::cerr << "use lines          : " << use_lines << std::endl;
  std::cerr << "use planes         : " << use_planes << std::endl;
  std::cerr << "use surfels        : " << use_surfels << std::endl;
  std::cerr << "use support weights: " << use_support_weight << std::endl;
  std::cerr << "use cross constrs  : " << use_cc << std::endl;
  std::cerr << "config             : " << yaml_config << std::endl;
  std::cerr << "verbosity          : " << verbose << std::endl;
  std::cerr << "time stats         : " << time_stats << std::endl;
  std::cerr << "gt_file            : " << gt_file << std::endl;
  std::cerr << "skip_frame         : " << skip_frames << std::endl;
  std::cerr << "use gui            : " << use_gui << std::endl;
  std::cerr << "show images        : " << show_images << std::endl;
  std::cerr << "show matchables    : " << show_matchables << std::endl;

  std::ofstream writer(output);
  reader.open(filename);

  MessageTimestampSynchronizer synchronizer;

  synchronizer.setTopics(depth_plus_rgb_topic);
  synchronizer.setTimeInterval(0.03);

  RGBImage rgb_image;
  RawDepthImage depth_image;

  ShapesTracker tracker;
  tracker.setup();
  tracker.aligner().mutableConfig().use_direct_solver = use_direct_solver;
  tracker.aligner().mutableConfig().use_support_weight = use_support_weight;
  tracker.aligner().mutableConfig().use_cross_constraints = use_cc;
  
  tracker.aligner().solver().setPointKernelThreshold(0.01);
  tracker.aligner().solver().setDirectionKernelThreshold(0.01);
  tracker.setVerbosity(verbose);

  srrg_matchable_detector::MatchableDetector::Config& matchable_config = tracker.detector().mutableConfig();
  matchable_config.detect_points  = use_points;
  matchable_config.detect_lines   = use_lines;
  matchable_config.detect_planes  = use_planes;
  matchable_config.detect_surfels = use_surfels;
  
  initDetectorFromYAML(yaml_config, tracker.detector());

  // get Time stats
  const ShapesTracker::TimeStats& tracker_time_stats = tracker.timeStats();
  const ShapesAligner::TimeStats& aligner_time_stats = tracker.aligner().timeStats();
  const srrg_matchable_detector::MatchableDetector::TimeStats& detector_time_stats = tracker.detector().timeStats();
  
  bool K_set = false;

  QApplication app(argc, argv);
  TrackerViewer viewer;
  if(use_gui) {
    viewer.setTracker(&tracker);
    viewer.show();
  }
  bool& play = viewer.play();
  bool& forward = viewer.forward();
  
  const int max_cameras = 300;
  int drawn_cameras = 0;
  
  ofstream snap_stream;
  int snap_count = 0;
  bool& save_snapshot = viewer.saveSnap();
  
  ofstream det_stream;
  int det_count = 0;

  viewer.setSnapshotQuality(100);

  Eigen::Isometry3f last_camera = Eigen::Isometry3f::Identity();

  srrg_rpe::StampVector6fPairVector estimated_trajectory;
  srrg_rpe::StampVector6fPairVector gt_trajectory;
  if(!gt_file.empty())
    srrg_rpe::fileToVector(gt_trajectory,
                           gt_file);
  
  
  int frame_count = 0;
  int count=0;
  
  while (reader.good()){
    BaseMessage* msg = reader.readMessage();
    if (!msg)
      continue;

    BaseImageMessage* base_img = dynamic_cast<BaseImageMessage*>(msg);
    if (!base_img) {
      delete msg;
      continue;
    }

    synchronizer.putMessage(base_img);

    if (!synchronizer.messagesReady())
      continue;

    PinholeImageMessage *depth_msg = 0, *rgb_msg = 0;

    depth_msg = dynamic_cast<PinholeImageMessage*>(synchronizer.messages()[0].get());
    if (!depth_msg)
      throw std::runtime_error("depth msg expected. Shall thou burn in hell.");

    rgb_msg = dynamic_cast<PinholeImageMessage*>(synchronizer.messages()[1].get());

    if (!depth_msg || !rgb_msg)
      continue;

    if(K_set && frame_count < skip_frames ){
      frame_count++;
      continue;
    }
    frame_count = 0;
    
    // get images
    depth_msg->image().copyTo(depth_image);
    rgb_msg->image().copyTo(rgb_image);

    if(show_images) {
      cv::imshow("rgb_image",rgb_image);
      if(!use_gui)
        cv::waitKey(10);
    }

    // camera matrix
    if(!K_set){
      const Eigen::Matrix3f K = depth_msg->cameraMatrix();
      tracker.detector().setK(K);
      tracker.detector().setRowsAndCols(rgb_image.rows,rgb_image.cols);
      tracker.detector().init();
      tracker.detector().setShowDetections(show_matchables);
      K_set = true;
    }

    tracker.setImages(rgb_image,depth_image);
    tracker.compute();

    // print Stats
    if(time_stats) {
      printTimeStats(tracker_time_stats,
                     aligner_time_stats,
                     detector_time_stats);
    }

    if(show_matchables) {
      cv::imshow("matchables",tracker.detector().detectionsImage());
      if(!use_gui)
        cv::waitKey(10);
    }

    count++;
    
    std::cerr << "T: " << srrg_core::t2v(tracker.globalT()).transpose() << std::endl;
    writer << std::setprecision(std::numeric_limits<double>::digits10)
           << rgb_msg->timestamp() << " ";
    writer << std::setprecision(std::numeric_limits<float>::digits10)
           << t2vFull(tracker.globalT()).transpose() << endl;

    srrg_core::Vector6f float_vector_madre = srrg_core::t2v(tracker.globalT());
    srrg_core::Vector6d double_vector_madre = float_vector_madre.cast<double>();
    
    estimated_trajectory.push_back(std::make_pair(rgb_msg->timestamp(),
                                                  double_vector_madre));
    
    if(!viewer.cameraPoses().size()) {
      viewer.cameraPoses().at(drawn_cameras%max_cameras) = tracker.globalT();
      drawn_cameras++;
      viewer.toDrawCameras()++;
      last_camera = tracker.globalT();
    }else {
      const Eigen::Isometry3f camera_step = last_camera.inverse() * tracker.globalT();
      if(camera_step.translation().squaredNorm() > 0.02) {
        viewer.cameraPoses().at(drawn_cameras%max_cameras) = tracker.globalT();
        viewer.toDrawCameras()++;
        drawn_cameras++;
        last_camera = tracker.globalT();
      }
    }
      
    if(save_snapshot) {
        if(!snap_stream) {
          snap_stream.open("snapshots/new_images.txt");
        }
        char snap_filename[100];
        sprintf(snap_filename, "snapshots/new_image-%07d.jpg", snap_count);
        snap_stream << snap_filename << endl;
        viewer.saveSnapshot(QString(snap_filename));
        snap_count++;

//      if(!snap_stream) {
//        snap_stream.open("snapshots/images.txt");
//      }
//      char snap_filename[100];
//      sprintf(snap_filename, "snapshots/image-%07d.jpg", snap_count);
//      snap_stream << snap_filename << endl;
//      viewer.saveSnapshot(QString(snap_filename));
//      snap_count++;

//      if(!det_stream) {
//        det_stream.open("snapshots/detections.txt");
//      }
//      char det_filename[100];
//      sprintf(det_filename, "snapshots/detetction-%07d.jpg", det_count);
//      det_stream << det_filename << endl;
//      cv::imwrite(det_filename,tracker.detector().detectionsImage());
//      det_count++;
    }

    if(use_gui) {
      viewer.updateGL();
      app.processEvents();
      
      while(!play){
        viewer.updateGL();
        app.processEvents();
        if(forward){
          forward = false;
          break;
        }
      }
    }
    
  }

  if(matchable_count!=""){
      std::cerr << "saving matchable count to file: " << matchable_count << std::endl;
      tracker.countMatchables(matchable_count);

    }

  const std::vector<double>& detection_timings = tracker.detector().timings();
  const std::vector<double>& association_timings = tracker.aligner().associationTimings();
  const std::vector<double>& solve_timings = tracker.aligner().solveTimings();
  int frames = detection_timings.size();
  double cum_det=0;
  double cum_ass=0;
  double cum_sol=0;
  for(int i=0; i<frames; ++i){
    cum_det += detection_timings[i];
    cum_ass += association_timings[i];
    cum_sol += solve_timings[i];
  }
  double avg_det=cum_det/(double)frames;
  double avg_ass=cum_ass/(double)frames;
  double avg_sol=cum_sol/(double)frames;
  cum_det=0;cum_ass=0;cum_sol=0;
  for(int i=0; i<frames; ++i){
    cum_det += fabs(detection_timings[i]-avg_det);
    cum_ass += fabs(association_timings[i]-avg_ass);
    cum_sol += fabs(solve_timings[i]-avg_sol);
  }
  double var_det=cum_det/(double)frames;
  double var_ass=cum_ass/(double)frames;
  double var_sol=cum_sol/(double)frames;
  std::cerr << "Detection time - avg:" << avg_det << " - var: " << var_det << std::endl;
  std::cerr << "Association time - avg:" << avg_ass << " - var: " << var_ass << std::endl;
  std::cerr << "Solve time - avg:" << avg_sol << " - var: " << var_sol << std::endl;

  srrg_rpe::RPE relative_error;
  if(!gt_file.empty())
    srrg_rpe::evaluateTrajectory(relative_error, gt_trajectory, estimated_trajectory);
  
  if(use_gui)
    app.exec();

  writer.close();
  return 0;
}



void initDetectorFromYAML(const std::string& yaml_file,
                          MatchableDetector& detector) {

  if(yaml_file.empty())
    return;

  MatchableDetector::Config& matchable_detector = detector.mutableConfig();
  PointDetector::Config& point_detector = detector.pointDetector().mutableConfig();
  LineDetector::Config& line_detector = detector.lineDetector().mutableConfig();
  PlaneDetector::Config& plane_detector = detector.planeDetector().mutableConfig();
  SurfelDetector::Config& surfel_detector = detector.surfelDetector().mutableConfig();
  
  YAML::Node configuration = YAML::LoadFile(yaml_file);

  matchable_detector.max_curvature = configuration["matchable_detector"]["max_curvature"].as<float>();
  
  point_detector.threshold = configuration["point_detector"]["threshold"].as<int>();
  point_detector.nonmaxSuppression = configuration["point_detector"]["nonmaxSuppression"].as<int>();

  line_detector.scale = configuration["line_detector"]["scale"].as<int>();
  line_detector.num_octaves = configuration["line_detector"]["num_octaves"].as<int>();
  line_detector.lp2t_threshold = configuration["line_detector"]["lp2t_threshold"].as<float>();
  line_detector.bp2d_threshold = configuration["line_detector"]["bp2d_threshold"].as<float>();

  plane_detector.min_cluster_points = configuration["plane_detector"]["min_cluster_points"].as<int>();

  surfel_detector.voxel_size = configuration["surfel_detector"]["voxel_size"].as<float>();
  
}


void printTimeStats(const ShapesTracker::TimeStats& tracker_time_stats,
                    const ShapesAligner::TimeStats& aligner_time_stats,
                    const srrg_matchable_detector::MatchableDetector::TimeStats& detector_time_stats) {

  std::cerr << FYEL("******************* Time Stats **********************") << std::endl;
  tracker_time_stats.print();
  aligner_time_stats.print();
  detector_time_stats.print();
  std::cerr << FYEL("*****************************************************") << std::endl; 
}
