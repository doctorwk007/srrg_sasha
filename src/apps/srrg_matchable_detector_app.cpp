#include <iostream>
#include <srrg_matchable_detector/matchable_detector.h>
#include <srrg_system_utils/system_utils.h>
#include <srrg_image_utils/depth_utils.h>
#include <srrg_image_utils/point_image_utils.h>
#include <srrg_system_utils/system_utils.h>
#include <srrg_messages/message_reader.h>
#include <srrg_messages/pinhole_image_message.h>
#include <srrg_messages/sensor_message_sorter.h>
#include <srrg_messages/message_timestamp_synchronizer.h>
#include <srrg_image_utils/depth_utils.h>

#include <yaml-cpp/yaml.h>

using namespace std;
using namespace srrg_core;
using namespace srrg_matchable;
using namespace srrg_matchable_detector;


void showStuff(const Scene&,const RGBImage&, const RGBImage&, const Eigen::Matrix3f&);
void showPoint(RGBImage&, MatchablePtr , const Eigen::Matrix3f&);
void showLine(RGBImage&, MatchablePtr , const Eigen::Matrix3f&);
void showPlane(RGBImage&, MatchablePtr , const Eigen::Matrix3f&);
void showSurfel(RGBImage&, MatchablePtr , const Eigen::Matrix3f&);

void yamlFromDetector(MatchableDetector& detector);

bool recompute;
int alpha_slider_max;
int alpha_slider;
double alpha;
double beta;

void onComputeTrackbar(int, void*){
  recompute = true;
}

const char* banner[] = {
  "matchable_detector_app: sasha grey in our hearts",
  "",
  "usage: exec-name <dump_file>",
  "-t    <string>              depth image topic name, as in the txtio file",
  "-rgbt <string>              rgb image topic name, as in the txtio file",
  "-npt <flag>                 do NOT detect points",
  "-nl <flag>                  do NOT detect lines",
  "-npl <flag>                 do NOT detect planes",
  "-ns <flag>                  do NOT detect surfels",
  0
};


int main(int argc, char** argv){

  if (argc < 2 || !strcmp(argv[1], "-h")) {
    printBanner(banner);
    return 0;
  }

  std::string filename = "";
  std::string depth_topic = "/camera/depth/image_raw";
  std::string rgb_topic = "/camera/rgb/image_raw";

  MessageReader reader;

  bool use_points  = true;
  bool use_lines   = true;
  bool use_planes  = true;
  bool use_surfels = true;
  
  std::vector<string> depth_plus_rgb_topic;
  
  int c = 1;
  while (c < argc) {
    if (!strcmp(argv[c], "-h")) {
      printBanner(banner);
      return 1;
    } else if (!strcmp(argv[c], "-t")) {
      c++;
      depth_topic = argv[c];
      depth_plus_rgb_topic.push_back(depth_topic);
    } else if (!strcmp(argv[c], "-rgbt")) {
      c++;
      rgb_topic = argv[c];
      depth_plus_rgb_topic.push_back(rgb_topic);
    } else if(!strcmp(argv[c], "-npt")) {
      use_points = false;
    } else if(!strcmp(argv[c], "-nl")) {
      use_lines = false;
    } else if(!strcmp(argv[c], "-npl")) {
      use_planes = false;
    } else if(!strcmp(argv[c], "-ns")) {
      use_surfels = false;      
    } else {
      filename = argv[c];
    }
    c++;
  }

  std::cerr << "depth_topic: " << depth_topic << std::endl;
  std::cerr << "rgb_topic  : " << rgb_topic << std::endl;
  std::cerr << "filename   : " << filename << std::endl;

  reader.open(filename);

  MessageTimestampSynchronizer synchronizer;

  synchronizer.setTopics(depth_plus_rgb_topic);
  synchronizer.setTimeInterval(0.03);

  RGBImage rgb_image;
  RawDepthImage depth_image;

  MatchableDetector detector;
  MatchableDetector::Config& detector_config = detector.mutableConfig();
  detector_config.detect_lines = use_lines;
  detector_config.detect_points = use_points;
  detector_config.detect_surfels = use_surfels;
  detector_config.detect_planes = use_planes;
  PointDetector::Config& point_config = detector.pointDetector().mutableConfig();
  LineDetector::Config& line_config = detector.lineDetector().mutableConfig();
  PlaneDetector::Config& plane_config = detector.planeDetector().mutableConfig();
  SurfelDetector::Config& surfel_config = detector.surfelDetector().mutableConfig();
  
  recompute = true;
  alpha_slider_max = 100;
  alpha=0.3;

  
  //Controls
  cv::namedWindow("controls");
  //Linear Blend
  cv::createTrackbar("alpha","controls",&alpha_slider,alpha_slider_max,onComputeTrackbar);

  /// Point Detector Parameters
  // Threshold
  cv::createTrackbar("[Pt]threshold","controls",&point_config.threshold,100,onComputeTrackbar);
  // NonMaxSuppression
  int nonmaxSuppression = point_config.nonmaxSuppression;  
  cv::createTrackbar("[Pt]nonmaxSuppression","controls",&nonmaxSuppression,1,onComputeTrackbar);

  /// Line Detector Parameters
    //Scale
  cv::createTrackbar("[L]scale","controls",&line_config.scale,20,onComputeTrackbar);
  //N bins
  cv::createTrackbar("[L]num_octaves","controls",&line_config.num_octaves,2,onComputeTrackbar);
  //LP2T Th
  int lp2t_th_in_mm = 1000*line_config.lp2t_threshold;
  cv::createTrackbar("[L]lp2t_th","controls",&lp2t_th_in_mm,100000,onComputeTrackbar);
  //BP2D Th
  int bp2d_th_in_mm = 1000*line_config.bp2d_threshold;
  cv::createTrackbar("[L]bp2d_th","controls",&bp2d_th_in_mm,100,onComputeTrackbar);

  /// Plane Detector Parameters
  //Min Cluster Points
  cv::createTrackbar("[Pl]min_cluster_points","controls",&plane_config.min_cluster_points,10000,onComputeTrackbar);

  int voxel_size = 1000*surfel_config.voxel_size;
  cv::createTrackbar("[S]voxel","controls", &voxel_size, 1000, onComputeTrackbar);

  //Max Curvature
  int max_curvature_in_mm = 100000*detector_config.max_curvature;
  cv::createTrackbar("[D]max_curvature","controls",&max_curvature_in_mm,100000,onComputeTrackbar);

  Scene matchable_bazar;
  
  bool K_set = false;
  Eigen::Matrix3f K;
  K.setIdentity();

  cv::Mat rgb_regions;

  while (reader.good()){
    BaseMessage* msg = reader.readMessage();
    if (!msg)
      continue;

    BaseImageMessage* base_img = dynamic_cast<BaseImageMessage*>(msg);
    if (!base_img) {
      delete msg;
      continue;
    }

    synchronizer.putMessage(base_img);

    if (!synchronizer.messagesReady())
      continue;

    PinholeImageMessage *depth_msg = 0, *rgb_msg = 0;

    depth_msg = dynamic_cast<PinholeImageMessage*>(synchronizer.messages()[0].get());
    if (!depth_msg)
      throw std::runtime_error("depth msg expected. Shall thou burn in hell.");

    rgb_msg = dynamic_cast<PinholeImageMessage*>(synchronizer.messages()[1].get());

    if (!depth_msg || !rgb_msg)
      continue;

    // get images
    depth_msg->image().copyTo(depth_image);
    rgb_msg->image().copyTo(rgb_image);

    // camera matrix
    if(!K_set){
      K = depth_msg->cameraMatrix();
      detector.setK(K);
      detector.setRowsAndCols(rgb_image.rows,rgb_image.cols);
      detector.init();
      K_set = true;
    }

    detector.setImages(depth_image, rgb_image);
    
    while(true) {

      if(recompute){
        
        detector_config.max_curvature = 1e-5*max_curvature_in_mm;
        detector.setImages(depth_image, rgb_image);
        
        point_config.nonmaxSuppression = nonmaxSuppression;
        line_config.lp2t_threshold = 1e-3*lp2t_th_in_mm;
        line_config.bp2d_threshold = 1e-3*bp2d_th_in_mm;

        surfel_config.voxel_size = 1e-3*voxel_size;
        
        detector.pointDetector().init();
        detector.lineDetector().init();
        detector.planeDetector().init();        
        
        matchable_bazar.clear();
        detector.compute(matchable_bazar);
        recompute = false;
      }

      cv::Mat regions = detector.regionsImage().clone();
      cv::Mat temp;
      regions.convertTo(temp,CV_8UC1,-255);    
      cv::cvtColor(temp,rgb_regions,CV_GRAY2BGR);
      cv::imwrite("regions.png",rgb_regions);

      showStuff(matchable_bazar, rgb_image,rgb_regions, K);

      unsigned char key=cv::waitKey(10);
      if(key == 32){ //SPACE
        recompute = true;
        break;
      }
      else if (key == 115) { // S, save
        yamlFromDetector(detector);
      }
    }
  }

  return 0;
}





void showStuff(const Scene& bazar,
               const RGBImage& rgb_image,
               const RGBImage& rgb_regions,
               const Eigen::Matrix3f& K) {

  RGBImage detection_image;
  rgb_image.copyTo(detection_image);
  RGBImage output_image;
  
  for(size_t i = 0; i < bazar.size(); ++i){

    MatchablePtr m = bazar[i];
    const Matchable::Type type = m->type();

    switch(type) {
    case Matchable::Point:
      showPoint(detection_image, m, K);
      break;
    case Matchable::Line:
      showLine(detection_image, m, K);
      break;
    case Matchable::Plane:
      showPlane(detection_image, m, K);
      break;
    case Matchable::Surfel:
      showSurfel(detection_image, m, K);
      break;
    default:
      break;
    }
  }

  cv::imwrite("detection.png",detection_image);

  alpha = (double) alpha_slider/alpha_slider_max ;
  beta = ( 1.0 - alpha );

  addWeighted( rgb_image, alpha, detection_image, beta, 0.0, output_image);

  RGBImage dest;
  dest.create(output_image.rows,output_image.cols+rgb_regions.cols);
  dest = cv::Vec3b (0,0,0);
  
  cv::Mat dest_rect = dest(cv::Rect(0,0,output_image.cols,output_image.rows));
  output_image.copyTo(dest_rect);

  dest_rect = dest(cv::Rect(rgb_regions.cols, 0, rgb_regions.cols,rgb_regions.rows));
  rgb_regions.copyTo(dest_rect);

  cv::imshow("controls", dest);



}

void showPoint(RGBImage& detection_image,
               MatchablePtr m,
               const Eigen::Matrix3f& K) {

  const Eigen::Vector3f& p = m->point();
  Eigen::Vector3f coord = K*p;
  coord /= coord(2);

  cv::circle(detection_image, cv::Point2f(coord.x(), coord.y()), 2, cv::Scalar( 0, 255, 0 ),2);
  
}

void showSurfel(RGBImage& detection_image,
                MatchablePtr m,
                const Eigen::Matrix3f& K) {

  const Eigen::Vector3f& p = m->point();
  Eigen::Vector3f coord = K*p;
  coord /= coord(2);

  cv::circle(detection_image, cv::Point2f(coord.x(), coord.y()), 2, cv::Scalar(  255, 0, 255 ),2);
  
}

void showLine(RGBImage& detection_image,
              MatchablePtr m,
              const Eigen::Matrix3f& K) {

  // project
  const Eigen::Vector3f& p = m->point();
  const Eigen::Vector3f& d = m->directionVector();
  const Eigen::Vector2f& e = m->extent();

  Eigen::Vector3f coordA = K*p;
  coordA /= coordA(2);

  Eigen::Vector3f q = p + d*e.x();
  Eigen::Vector3f coordB = K*q;
  coordB /= coordB(2);

  cv::line(detection_image,
           cv::Point (coordA.x(), coordA.y()),
           cv::Point (coordB.x(), coordB.y()),
           cv::Scalar (255,0,0),2); 
}

void showPlane(RGBImage& detection_image,
               MatchablePtr m,
               const Eigen::Matrix3f& K) {

  Eigen::Isometry3f transform;
  transform.setIdentity();
  transform.linear() = m->rotationMatrix();
  transform.translation() = m->point();
    
  // project
  const Cloud3D& cloud = m->cloud();
  std::vector<cv::Point> points;
  
  for(size_t i=0;i<cloud.size();++i){

    Eigen::Vector3f coord = K*transform*cloud[i].point();
    coord /= coord(2);
    points.push_back(cv::Point(coord.x(),coord.y()));
    cv::circle(detection_image,cv::Point2f(coord.x(),coord.y()),2,cv::Scalar(0,0,255),2);
  }
}


void yamlFromDetector(MatchableDetector& detector) {

  MatchableDetector::Config& matchable_detector = detector.mutableConfig();
  PointDetector::Config& point_detector = detector.pointDetector().mutableConfig();
  LineDetector::Config& line_detector = detector.lineDetector().mutableConfig();
  PlaneDetector::Config& plane_detector = detector.planeDetector().mutableConfig();
  SurfelDetector::Config& surfel_detector = detector.surfelDetector().mutableConfig();
  
  YAML::Emitter out;
  
  out << YAML::BeginMap;
  
  out << YAML::Key << "matchable_detector";

  out << YAML::BeginMap;
  out << YAML::Key << "max_curvature";
  out << YAML::Value << matchable_detector.max_curvature;
  out << YAML::EndMap;
  
  out << YAML::Key << "point_detector";
  
  out << YAML::BeginMap;
  out << YAML::Key << "threshold";
  out << YAML::Value << point_detector.threshold;
  out << YAML::Key << "nonmaxSuppression";
  out << YAML::Value << (int)point_detector.nonmaxSuppression;
  out << YAML::EndMap;

  out << YAML::Key << "line_detector";

  out << YAML::BeginMap;
  out << YAML::Key << "scale";
  out << YAML::Value << line_detector.scale;
  out << YAML::Key << "num_octaves";
  out << YAML::Value << line_detector.num_octaves;
  out << YAML::Key << "lp2t_threshold";
  out << YAML::Value << line_detector.lp2t_threshold;
  out << YAML::Key << "bp2d_threshold";
  out << YAML::Value << line_detector.bp2d_threshold;
  out << YAML::EndMap;
  
  out << YAML::Key << "plane_detector";
  
  out << YAML::BeginMap;
  out << YAML::Key << "min_cluster_points";
  out << YAML::Value << plane_detector.min_cluster_points;  
  out << YAML::EndMap;

  out << YAML::Key << "surfel_detector";
  
  out << YAML::BeginMap;
  out << YAML::Key << "voxel_size";
  out << YAML::Value << surfel_detector.voxel_size;
  out << YAML::EndMap;
  
  out << YAML::EndMap;
  
  std::ofstream fout("current.yaml");
  fout << out.c_str();
  std::cerr << "written a yaml file" << std::endl;
}
