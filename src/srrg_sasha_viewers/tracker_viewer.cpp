#include "tracker_viewer.h"
#include <cstring>
#include <qevent.h>

#include <srrg_gl_helpers/opengl_primitives.h>

using namespace srrg_gl_helpers;
using namespace srrg_matchable;

namespace srrg_sasha{

  bool zozzimma = false;

  void TrackerViewer::drawFloor(){
    glPolygonMode(GL_FRONT_AND_BACK,GL_LINE);

     float step = 0.5;
     int dim = 50;
     for(int j=0; j < dim; ++j)
       for(int i=0; i < dim; ++i){
           Eigen::Vector3f point(-5+i*step,-5+j*step,0);
           glColor4f(0.0,0.0,0.0,.75);
           Eigen::Isometry3f transform = Eigen::Isometry3f::Identity();
           transform.translation() = point;
           glPushMatrix();
           glMultMatrix(transform);

           GLfloat sx = step*0.5f;
           GLfloat sy = step*0.5f;

           glBegin(GL_QUADS);
           glNormal3f( 0.0f, 0.0f, 1.0f);
           glVertex3f(-sx, -sy, 0.f);
           glVertex3f(-sx, sy, 0.f);
           glVertex3f(sx, sy, 0.f);
           glVertex3f(sx, -sy, 0.f);
           glEnd();

           glPopMatrix();
         }
     glPolygonMode(GL_FRONT_AND_BACK,GL_FILL);

  }

  void TrackerViewer::draw(){
    _scene_fixed = _tracker->sceneFixed();
    _scene_moving = _tracker->sceneMoving();
  
//    srrg_core::Vector6f v;
//    v << 0.0f,1.5f,0.0f,0.6f,-0.6f,0.35f;
//    Eigen::Isometry3f t = srrg_core::v2t(v);

//    glPushMatrix();
//    glMultMatrix(t);
//    drawFloor();
//    glPopMatrix();

    glPushMatrix();
    if(!_follow_camera)
      glMultMatrix(_tracker->globalT());

    // -1 - draw Trail of cameras
    glPushMatrix();
    {
      glMultMatrix(_tracker->globalT().inverse());
      Eigen::Vector3f start_pose(Eigen::Vector3f::Zero());
      int k = 0;
      for(const Eigen::Isometry3f& camera : _camera_poses){

        glPushMatrix();
        glBegin(GL_LINES);
        {
          glColor4f(0.0, 0.0, 1.0, .2);
          glVertex3f(start_pose.x(), start_pose.y(), start_pose.z());
          start_pose = camera.translation();
          glVertex3f(start_pose.x(), start_pose.y(), start_pose.z());
        }
        glEnd();
        glPopMatrix();
      
        glPushMatrix();
        {
          glMultMatrix(camera);
          glColor4f(1.0, 0.0, 0.0, .2);
          glPushMatrix();
          {
            drawPyramidWireframe();
          }
          glPopMatrix();
        }
        glPopMatrix();

        if(k == _to_draw_cameras-1)
          break;
        ++k;
      }
    }
    glPopMatrix();
    
    // 0 - draw Map
    glPushMatrix();
    glMultMatrix(_tracker->globalT().inverse());
    _matchable_store->draw();    
    glPopMatrix();
    
    // 1 - draw camera reference system
    glPushMatrix();
    glColor4f(.5, 0.0, 0.0, 1.0);
    drawPyramidWireframe();      
    glPopMatrix();    
    
    // 2 - draw the fixed scene (the one accumulating)
    glPushMatrix();
    if(_scene_fixed){
      _scene_fixed->draw();
    }
    glPopMatrix();
   
    
    glPopMatrix();    
  }
  
  void TrackerViewer::keyPressEvent(QKeyEvent *event) {
    bool verbosity = _tracker->verbosity();
    
    switch(event->key()) {

    case Qt::Key_P:
      _play ^= 1;
      _play ? std::cerr << FCYN("[VIEWER]: Play") : std::cerr << FCYN("[VIEWER]: Pause");
      std::cerr << std::endl;
      break;

    case Qt::Key_C:
      _follow_camera ^= 1;
      std::cerr << FCYN("[VIEWER]: Follow Camera switch") << std::endl;
      break;

    case Qt::Key_S:
      _save_snapshot ^=1;
      std::cerr << FCYN("[VIEWER]: Save Snapshot switch") << std::endl;     
      break;      
      
    case Qt::Key_F:
      _play = false;
      _forward ^= 1;
      std::cerr << FCYN("[VIEWER]: Forward") << std::endl;
      break;
      
    case Qt::Key_V:
      verbosity ^= 1;
      _tracker->setVerbosity(verbosity);
      std::cerr << FCYN("[VIEWER][KeyPress] Changed Verbosity to: ") << verbosity<< std::endl;
      break;
    case Qt::Key_I:
      std::cerr << FCYN("[VIEWER][Info] fixed_size: ") << _scene_fixed->size()
                << FCYN(" moving_size: ") << _scene_moving->size() << std::endl;
      break;

    case Qt::Key_H:
      std::cerr << FCYN("[Viewer] HELP:") << std::endl;
      std::cerr << FGRN("KEY / EFFECT") << std::endl;
      std::cerr << KCYN;
      std::cerr << "P:  Play/Pause" << std::endl;
      std::cerr << "F:  Forward (1 frame)" << std::endl;
      std::cerr << "C:  Follow/UnFollow Camera" << std::endl;
      std::cerr << "S:  Save/UnSave Snapshot" << std::endl;
      std::cerr << "V:  Verbose on/off" << std::endl;
      std::cerr << "I:  Scene Info" << std::endl;
      std::cerr << RST;
      break;
      
    default: QGLViewer::keyPressEvent(event);
    }
    updateGL();
  }

}
