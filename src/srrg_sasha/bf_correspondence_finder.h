#pragma once

#include <srrg_matchable/scene.h>
#include "base_registration_solver.h"
#include <srrg_system_utils/system_utils.h>

#include <iostream>
#include <srrg_system_utils/colors.h>

namespace srrg_sasha {

  using namespace srrg_matchable;

  class BFCorrespondenceFinder {
  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;

    struct Config{
      float error_threshold = 0.01;
    };

    // c'tor
    BFCorrespondenceFinder():_fixed(NULL),
      _moving(NULL),
      _fixed_size(0),
      _moving_size(0),
      _verbose(false),
      _un_associated(0){
      _configuration = new Config();      
    }
    
    ~BFCorrespondenceFinder() {      
      std::cerr << FRED("[BF-CORRESPONDENCE-FINDER]: ");
      delete _configuration;
      std::cerr << FRED(" deleted") << std::endl;
    }

    void setScenes(Scene* fixed_, Scene* moving_){
      _fixed = fixed_;
      _fixed_size = _fixed->size();
      _moving = moving_;
      _moving_size = _moving->size();
    }

    void setVerbosity(bool verbose_){_verbose = verbose_;}

    void init();

    // query the three with the moving
    void compute(BaseRegistrationSolver::ConstraintVector& constraints_,
                 const Eigen::Isometry3f& T_ = Eigen::Isometry3f::Identity());

    Config& mutableConfig(){return *_configuration;}

    const int unAssociated() const {return _un_associated;}

  private:
    Config* _configuration;
    Scene* _moving;
    Scene* _fixed;
    int _fixed_size;
    int _moving_size;
    int _un_associated; // a counter for track broken help
    bool _verbose;
  };
  
}
