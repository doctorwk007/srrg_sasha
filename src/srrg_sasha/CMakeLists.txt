add_library(srrg_sasha_library SHARED
  shapes_aligner.cpp shapes_aligner.h
  data_association.cpp data_association.h
  nn_correspondence_finder.cpp nn_correspondence_finder.h
  bf_correspondence_finder.cpp bf_correspondence_finder.h
  shapes_tracker.cpp shapes_tracker.h
  base_registration_solver.cpp base_registration_solver.h
  direct_registration_solver.cpp direct_registration_solver.h
  iterative_registration_solver.cpp iterative_registration_solver.h
  )

target_link_libraries(srrg_sasha_library
  srrg_matchable_detector_library
  ${catkin_LIBRARIES}
)
