#pragma once
#include <srrg_matchable/scene.h>
#include "iterative_registration_solver.h"
#include "direct_registration_solver.h"

#include <srrg_system_utils/system_utils.h>
#include <srrg_system_utils/colors.h>

#include "nn_correspondence_finder.h"
#include "bf_correspondence_finder.h"

namespace srrg_sasha{

  class ShapesAligner{
  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;


    struct Config{
      bool use_direct_solver = false;
      bool use_support_weight = false;
      bool use_cross_constraints = false;
    };
    struct TimeStats{
      float data_association = -1.f;
      float solve = -1.f;
      void print() const;
      std::string write() const;
    };
    
  ShapesAligner():_scene_fixed(NULL),
      _scene_moving(NULL),
      _T(Eigen::Isometry3f::Identity()),
      _verbose(false){
      _configuration = new Config();
    }


    Config& mutableConfig() {return *_configuration;}
    const TimeStats& timeStats() const {return _time_stats;}
    
    void setMaxIterations(const int max_iterations_){_max_iterations = max_iterations_;}

    void setFixedScene(srrg_matchable::Scene* scene_fixed_){_scene_fixed = scene_fixed_;}
    void setMovingScene(srrg_matchable::Scene* scene_moving_){_scene_moving = scene_moving_;}
    void setT(const Eigen::Isometry3f& T_){_T = T_;}
    void setVerbosity(const bool verbose_){_verbose = verbose_;
      _nn_ass.setVerbosity(verbose_);
      _bf_ass.setVerbosity(verbose_);
    }
    
    void compute(const Eigen::Isometry3f& init_guess_ = Eigen::Isometry3f::Identity());

    const Eigen::Isometry3f& T() const {return _T;}
    const bool verbosity() const {return _verbose;}

    inline BaseRegistrationSolver& solver(){
      if(_configuration->use_direct_solver) return _direct_solver; return _iterative_solver;}

    const BFCorrespondenceFinder& bfCorrespondenceFinder() const {return _bf_ass;}
    const NNCorrespondenceFinder& nnCorrespondenceFinder() const {return _nn_ass;}

    const std::vector<double>& associationTimings() const {return _association_timings;}
    const std::vector<double>& solveTimings() const {return _solve_timings;}


  protected:
    void computeDirect(const Eigen::Isometry3f& init_guess_ = Eigen::Isometry3f::Identity());
    
    Config *_configuration;
    TimeStats _time_stats;

    BFCorrespondenceFinder _bf_ass;
    NNCorrespondenceFinder _nn_ass;

    IterativeRegistrationSolver _iterative_solver;
    DirectRegistrationSolver _direct_solver;
    int _max_iterations;
    srrg_matchable::Scene* _scene_fixed;
    srrg_matchable::Scene* _scene_moving;
    Eigen::Isometry3f _T;
    bool _verbose;

    std::vector<double> _association_timings;
    std::vector<double> _solve_timings;
  };
}
