#pragma once
#include <srrg_matchable/matchable.h>
#include "base_registration_solver.h"

namespace srrg_sasha{
  class IterativeRegistrationSolver : public BaseRegistrationSolver {
  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;

    // constructs an empty solver
    IterativeRegistrationSolver();

    //
    ~IterativeRegistrationSolver();

    // prepares the optimization, starting from the specified pose
    virtual void init(const Eigen::Isometry3f& transform_=Eigen::Isometry3f::Identity(),
                      const bool use_support_weight = false);
    
    // executes one round of optimization
    virtual void oneRound();

    float chiSquare(){return _chi_square/(float)_num_inliers;}
    int numInliers(){return _num_inliers;}

  protected:

    // computes the error and the Jacobian of a constraint
    inline void errorAndJacobian(Vector3f& e_p,    // point-error
                                 Vector3f& e_d,    // direction-error
                                 float& e_o,              // orthogonality error
                                 float& e_x,              // intersection error
                                 Matrix3_6f& J_p,  // point jacobian
                                 Matrix3_6f& J_d,  // direction jacobian
                                 Matrix1_6f& J_o,  // orthogonality jacobian
                                 Matrix1_6f& J_x,  // orthogonality error
                                 bool compute_p,          // if true computes the point part
                                 bool compute_d,          // if true computes the direction part
                                 bool compute_o,          // if true computes the orthogonality part
                                 bool compute_x,          // if true computes the orthogonality part
                                 const srrg_matchable::MatchablePtr& fixed, // input fixed item
                                 const srrg_matchable::MatchablePtr& moving, // input moving item
                                 const bool rotate_omega = false); 

    // generic constraint linearization
    // side effect on _H, _b and the constrant vector to write the error
    // of each constraint
    inline void linearizeConstraint(Constraint& constraint);    // set to true if the constraint is an intersection

    Matrix6f _H;
    Vector6f _b;

    float _chi_square;
    int _num_inliers;

  };
  
}
