#pragma once

#include <srrg_matchable/scene.h>
#include "base_registration_solver.h"
#include <srrg_kdtree/kd_tree.hpp>
#include <srrg_system_utils/system_utils.h>

#include <iostream>
#include <srrg_system_utils/colors.h>

namespace srrg_sasha {

  using namespace srrg_matchable;
  
#define KDTREE_DIM_POINT  3
#define KDTREE_DIM_LINE   6
#define KDTREE_DIM_PLANE  4 
#define KDTREE_DIM_SURFEL 6
  
  class NNCorrespondenceFinder{
  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;

    struct Config{
      float direction_scale = 1;
      float normal_scale = 10;
      float leaf_range = 0.9;
      float leaf_range_surfel = 0.2;
      int hamming_point_threshold = 15;
      int hamming_line_threshold = 25;
    };
    
    // c'tor
    NNCorrespondenceFinder():_fixed(NULL),
      _moving(NULL),
      _kd_tree_point(NULL),
      _kd_tree_line(NULL),
      _kd_tree_plane(NULL),
      _kd_tree_surfel(NULL),
      _fixed_size(0),
      _moving_size(0),
      _verbose(false),
      _un_associated(0){
      _associations = new Scene();
      _configuration = new Config();      
    }
    
    // d'tor
    ~NNCorrespondenceFinder() {
      std::cerr << FRED("[NN-CORRESPONDENCE-FINDER]: deleting") << std::endl;
      if(_kd_tree_point)
        delete _kd_tree_point;
      if(_kd_tree_line)
        delete _kd_tree_line;
      if(_kd_tree_plane)
        delete _kd_tree_plane;
      if(_kd_tree_surfel)
        delete _kd_tree_surfel;
      delete _associations;
      delete _configuration;
      std::cerr << BOLD(FRED("[NN-CORRESPONDENCE-FINDER]: deleting")) << std::endl;
    }

    void setScenes(Scene* fixed_, Scene* moving_){
      _fixed = fixed_;
      _fixed_size = _fixed->size();
      _moving = moving_;
      _moving_size = _moving->size();
    }

    void setVerbosity(bool verbose_){_verbose = verbose_;}
    
    Scene* associations() {return _associations;}
    
    // initializes the tree from the fixed
    void init();

    // query the three with the moving
    void compute(BaseRegistrationSolver::ConstraintVector& constraints_,
                 const Eigen::Isometry3f& T_ = Eigen::Isometry3f::Identity());

    Config& mutableConfig(){return *_configuration;}

    const int unAssociated() const {return _un_associated;}
    
  protected:

    // write the fixed scene as a VectorTDVector. Called in the init,
    // during the tree construction
    void model2linear();

  private:

    Config* _configuration;
    
    srrg_core::KDTree<float, KDTREE_DIM_POINT>* _kd_tree_point;
    srrg_core::KDTree<float, KDTREE_DIM_POINT>::VectorTDVector _reference_matchable_points;
    std::vector<int> _reference_point_map;
    
    srrg_core::KDTree<float, KDTREE_DIM_LINE>* _kd_tree_line;
    srrg_core::KDTree<float, KDTREE_DIM_LINE>::VectorTDVector _reference_matchable_lines;
    std::vector<int> _reference_line_map;
    
    srrg_core::KDTree<float, KDTREE_DIM_PLANE>* _kd_tree_plane;
    srrg_core::KDTree<float, KDTREE_DIM_PLANE>::VectorTDVector _reference_matchable_planes;
    std::vector<int> _reference_plane_map;

    srrg_core::KDTree<float, KDTREE_DIM_SURFEL>* _kd_tree_surfel;
    srrg_core::KDTree<float, KDTREE_DIM_SURFEL>::VectorTDVector _reference_matchable_surfels;
    std::vector<int> _reference_surfel_map;
   
    Scene* _moving;
    Scene* _fixed;

    int _fixed_size;
    int _moving_size;

    int _un_associated; // a counter for track broken help
    
    // owns this scene
    Scene* _associations;

    bool _verbose;
    
  };


}
