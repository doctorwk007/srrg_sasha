#pragma once

#include <srrg_system_utils/system_utils.h>
#include <srrg_types/defs.h>
#include <Eigen/Dense>

#include <srrg_system_utils/colors.h>
#include <iostream>

namespace srrg_rpe {

  using namespace srrg_core;
  
  typedef std::pair<double, srrg_core::Vector6d> StampVector6fPair;
  typedef std::vector<StampVector6fPair> StampVector6fPairVector;
  
  struct RelativeError{
    double rmse   = 0.f;
    double mean   = 0.f;
    double median = 0.f;
    double std    = 0.f;
    double min    = 0.f;
    double max    = 0.f;
  };
  
  struct RPE{
    int compared_pose_pairs = 0;
    RelativeError translational_error;
    RelativeError rotational_error;
  };

  struct errorSample{
    double est0;
    double est1;
    double gt0;
    double gt1;
    double trans;
    double rot;
  };
  
  enum DeltaUnit{Seconds = 0x0, Meters = 0x1, Radians = 0x2, Frames = 0x3};



  int findClosest(const StampVector6fPairVector& ordered_trajectory,
                  const double& time_value) {
    int beginning = 0;
    double difference = std::fabs(ordered_trajectory[0].first - time_value);
    int best = 0;
    int end = ordered_trajectory.size();
    while(beginning < end) {
      int middle = int((end+beginning)/2);
      if(std::fabs(ordered_trajectory[middle].first - time_value) < difference) {
        difference = std::fabs(ordered_trajectory[middle].first - time_value);
        best = middle;        
      }
      if( time_value == ordered_trajectory[middle].first)
        return middle;
      else if(ordered_trajectory[middle].first > time_value)
        end = middle;
      else
        beginning = middle + 1;
    }
    return best;
  }


  bool sortFunction (const StampVector6fPair& i,
                     const StampVector6fPair& j) {
    return (i.first < j.first);
  }


  double computeAngle(const Eigen::Isometry3d& T) {   
    //    # an invitation to 3-d vision, p 27
    double rotation_trace = T.rotation().trace();    
    return acos(std::min(double(1), std::max(double(-1), (rotation_trace - 1)/2)));
  }
  
  /* 
  **FROM THE EVALUATE_RPE.PY by Sturm (TUM DATASET SUITE)**
    Compute the relative pose error between two trajectories.    
    Input:
    traj_gt -- the first trajectory (ground truth)
    traj_est -- the second trajectory (estimated trajectory)
    param_max_pairs -- number of relative poses to be evaluated
    param_fixed_delta -- false: evaluate over all possible pairs
    true: only evaluate over pairs with a given distance (delta)
    param_delta -- distance between the evaluated pairs
    param_delta_unit -- unit for comparison:
       "s": seconds
       "m": meters
       "rad": radians
       "deg": degrees
       "f": frames
    param_offset -- time offset between two trajectories (to model the delay)
    param_scale -- scale to be applied to the second trajectory    
    Output:
    list of compared poses and the resulting translation and rotation error
  */
  void evaluateTrajectory(RPE& result_rpe,
                          StampVector6fPairVector& ground_truth_,
                          StampVector6fPairVector& estimated_,
                          const int max_pairs = 10000,
                          const bool fixed_delta = false,
                          const double& delta = 1.f,
                          const DeltaUnit& delta_unit_ = DeltaUnit::Seconds,
                          const double& offset = 0.f,
                          const double& scale = 1.f) {

    // sort the two trajectories
    std::sort(ground_truth_.begin(), ground_truth_.end(), sortFunction);
    std::sort(estimated_.begin(), estimated_.end(), sortFunction);

    std::vector<std::pair<StampVector6fPair, StampVector6fPair> > associations;
    
    for(size_t i = 0; i < estimated_.size(); ++i) {     
      int gt_idx = findClosest(ground_truth_, estimated_[i].first + offset);
      associations.push_back(std::make_pair(estimated_[i], ground_truth_[gt_idx]));
    }

    std::vector<errorSample> result;
    
    double start_time = (associations[0].first).first;
    srrg_core::Vector6d est0 = (associations[0].first).second;
    srrg_core::Vector6d gt0 = (associations[0].second).second;
    for(size_t i = 1; i < associations.size(); ++i) {

      const double curr_time = (associations[i].first).first;
      if((curr_time - start_time) - delta < 1e-3) {        
        
        srrg_core::Vector6d est1 = (associations[i].first).second;
        srrg_core::Vector6d gt1 = (associations[i].second).second;
        Eigen::Isometry3d estDiff = v2t(est0).inverse() * v2t(est1);
        Eigen::Isometry3d gtDiff = v2t(gt0).inverse() * v2t(gt1);
        Eigen::Isometry3d error = estDiff.inverse() * gtDiff;
        
        errorSample sample;
        sample.trans = error.translation().norm();
        sample.rot = computeAngle(error);

        result.push_back(sample);
        
        est0 = est1;
        gt0 = gt1;
        start_time = curr_time;
      }
      
    }
    
    
    // compute statistic stuff
    result_rpe.compared_pose_pairs = result.size();
    std::cerr << "compared_pose_pairs: " << result_rpe.compared_pose_pairs << std::endl;

    double sq_sum = 0.f, sum = 0.f, rot_sq_sum = 0.f, rot_sum = 0.f;
    double min = 1e3f, max = -1e3f, rot_min = 1e3f, rot_max = -1e3f;
    for(size_t i = 0; i < result.size(); ++i) {
      const double& trans = result[i].trans;
      const double& rot = result[i].rot;
      sq_sum += trans*trans;
      rot_sq_sum += rot*rot;
      sum += trans;
      rot_sum += rot;
      if(trans > max)
        max = trans;
      if(trans < min)
        min = trans;
      if(rot > rot_max)
        rot_max = rot;
      if(rot < rot_min)
        rot_min = rot;
    }

    result_rpe.translational_error.rmse = sqrt(sq_sum/result.size());
    std::cerr << KGRN << "translational_error.rmse: " << result_rpe.translational_error.rmse << RST << std::endl;
    result_rpe.translational_error.mean = sum/result.size();
    std::cerr << "translational_error.mean: " << result_rpe.translational_error.mean << std::endl;
    result_rpe.translational_error.min = min;
    std::cerr << "translational_error.min: " << result_rpe.translational_error.min << std::endl;
    result_rpe.translational_error.max = max;
    std::cerr << "translational_error.max: " << result_rpe.translational_error.max << std::endl;

    result_rpe.rotational_error.rmse = sqrt(rot_sq_sum/result.size()) * 180.0 / M_PI;
    std::cerr << KGRN << "rotational_error.rmse: " << result_rpe.rotational_error.rmse << RST <<std::endl;
    result_rpe.rotational_error.mean = rot_sum/result.size() * 180.0 / M_PI;
    std::cerr << "rotational_error.mean: " << result_rpe.rotational_error.mean << std::endl;
    result_rpe.rotational_error.min = rot_min * 180.0 / M_PI;
    std::cerr << "rotational_error.min: " << result_rpe.rotational_error.min << std::endl;
    result_rpe.rotational_error.max = rot_max * 180.0 / M_PI;
    std::cerr << "rotational_error.max: " << result_rpe.rotational_error.max << std::endl;
    
    
  }


  void fileToVector(StampVector6fPairVector& res_,
                    const std::string& gt_file) {


    std::cerr << std::fixed;
    std::ifstream inputfile;
    inputfile.open(gt_file);
    if (!inputfile.is_open()) {
      std::cerr << "***[error]: file path not valid. Error in opening ***" << std::endl;
      std::exit(-1);
    }
    std::string line, prefix;
    while (!inputfile.eof()) {
      getline(inputfile, line);
      
      if (line.length() != 0 && line[0] != '#') //discard line beginning with '#'
        {
          std::istringstream ss(line);
          std::vector<double> values(7, 0.f);
          ss >> values[0]
             >> values[1]
             >> values[2]
             >> values[3]
             >> values[4]
             >> values[5]
             >> values[6];

          
          StampVector6fPair sample;
          sample.first = values[0];
          for(int i=0; i < 6; ++i)
            (sample.second)(i) = values[i+1];

          res_.push_back(sample);
          
        }
      
      
    }

    inputfile.close();
    
  }
  
}
