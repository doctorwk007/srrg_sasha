#pragma once

#include <srrg_matchable/scene.h>
#include "base_registration_solver.h"

namespace srrg_sasha {

  class DataAssociation{
  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW

    enum Type{BruteForce=0x0, Projective=0x1};

    DataAssociation():_scene_associations(new srrg_matchable::Scene()){}
    
    ~DataAssociation(){
      delete _scene_associations;
    }

    void associate(Type type_,
                   BaseRegistrationSolver::ConstraintVector& constraints,
                   srrg_matchable::Scene* scene_fixed,
                   srrg_matchable::Scene* scene_moving,
                   const Eigen::Isometry3f& transform = Eigen::Isometry3f::Identity());
    
    srrg_matchable::Scene* associations(){return _scene_associations;}
    void setAssociationsScene(srrg_matchable::Scene* scene_associations_){_scene_associations = scene_associations_;}
    
  protected:
    void associateWBruteForce(BaseRegistrationSolver::ConstraintVector& constraints,
                              srrg_matchable::Scene* scene_fixed,
                              srrg_matchable::Scene* scene_moving,
                              const Eigen::Isometry3f& transform = Eigen::Isometry3f::Identity());

    void associateWProjection(BaseRegistrationSolver::ConstraintVector& constraints,
                              srrg_matchable::Scene* scene_fixed,
                              srrg_matchable::Scene* scene_moving,
                              const Eigen::Isometry3f& transform = Eigen::Isometry3f::Identity());
    

  private:
    srrg_matchable::Scene* _scene_associations;
    
  };
}
