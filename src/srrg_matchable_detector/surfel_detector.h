#pragma once
#include <srrg_matchable/matchable.h>
#include <srrg_matchable/scene.h>
#include "plane_detector.h"

#include <srrg_system_utils/colors.h>

#include <Eigen/Geometry>

namespace srrg_matchable_detector {

  class SurfelDetector{
  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
    struct Config{
      float voxel_size = 0.05f;
    };
    
    SurfelDetector();
    ~SurfelDetector();
    
    Config& mutableConfig(){return _configuration;}

    void init();
    
    void  compute(srrg_matchable::Scene& matchables_,
                  const srrg_core::Float3Image& points_image_,
                  const srrg_core::Float3Image& normals_image_,
                  const srrg_core::IntImage& regions_image);

  private:
    Config _configuration;
    bool _initialized;
  };
  

}
