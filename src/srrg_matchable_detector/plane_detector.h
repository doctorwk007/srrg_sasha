#pragma once
#include <srrg_matchable/matchable.h>
#include <srrg_matchable/scene.h>

#include <srrg_system_utils/colors.h>

#include <iostream>
#include <string>
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include <srrg_image_utils/depth_utils.h>
#include <srrg_image_utils/point_image_utils.h>
#include <srrg_system_utils/system_utils.h>
#include <srrg_path_map/clusterer_path_search.h>
#include "srrg_types/cloud_3d.h"

#include <Eigen/Geometry>
#include <Eigen/Eigenvalues>


namespace srrg_matchable_detector{

  class PlaneDetector{
  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
    enum PixelType {Surfel=0,Line=-1,Plane=2};
    struct Config{
      int min_cluster_points = 1000;
    };

    PlaneDetector();
    Config& mutableConfig(){return _configuration;}

    
    void init();
    void compute(srrg_matchable::Scene& matchables_,
                 srrg_core::IntImage& regions_image,
                 const srrg_core::Float3Image& points_image,
                 const srrg_core::Float3Image& normals_image,
                 const srrg_core::FloatImage& curvature_image);

    inline const srrg_core::RGBImage& planesImage() const {return _planes_image;}

    void computePlaneRotationMatrix(Eigen::Matrix3f& rotation_matrix,
                                    const Eigen::Vector3f& direction);

  private:
    void extractClusters(srrg_core::ClustererPathSearch::ClusterVector& clusters,
                         const srrg_core::IntImage& regions_image);

    void generatePlanefromCluster(srrg_matchable::Scene& matchables_,
                                  srrg_core::IntImage& regions_image,
                                  const srrg_core::Float3Image& points_image,
                                  const srrg_core::Float3Image& normals_image,
                                  const srrg_core::ClustererPathSearch::Cluster& cluster_);

    Config _configuration;
    bool _initialized;
    srrg_core::RGBImage _planes_image;
    
  };
}
