#pragma once
#include <srrg_matchable/matchable.h>
#include <srrg_matchable/scene.h>

#include <srrg_system_utils/colors.h>

#include "opencv2/core/core.hpp"
#include "opencv2/core/utility.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui/highgui.hpp"

#include <opencv2/line_descriptor.hpp>

#include "srrg_types/cloud_3d.h"
#include <srrg_image_utils/depth_utils.h>

#include <Eigen/Geometry>
#include <Eigen/Eigenvalues>


namespace srrg_matchable_detector{

  class LineDetector{
  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;

    struct Config{     
      int scale = 2;
      int num_octaves = 1;
      float lp2t_threshold = 50;
      float bp2d_threshold = 5e-3f;
      int width_of_band = 14;
      float distance_resolution = 1.0f;
      float angle_resolution = 180.0f;
      int vote_threshold = 150;
      double min_line_length = 50;
      double max_line_gap = 0;
    };

    LineDetector();

    Config& mutableConfig(){return _configuration;}

    void init();
    
    void compute(srrg_matchable::Scene& matchables_,
                 const srrg_core::UnsignedCharImage& gray_image_,
                 const srrg_core::Float3Image& points_image_,
                 const srrg_core::FloatImage& curvature_image_ = srrg_core::FloatImage ());
  private:

    bool lp2t(const int num_of_pixels);

    bool bp2d(const cv::Vec3f& pA,
              const cv::Vec3f& pB,
              const cv::Vec3f& pC);

    void computeLineRotationMatrix(Eigen::Matrix3f& rotation_matrix,
                                    const Eigen::Vector3f& direction);


    Config _configuration;
    bool _initialized;
    bool _good_lines;
    cv::Ptr<cv::line_descriptor::LSDDetector> _ls;
    cv::Ptr<cv::line_descriptor::BinaryDescriptor> _bd;
    
  };
}
