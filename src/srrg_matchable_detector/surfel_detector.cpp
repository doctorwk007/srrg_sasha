#include "surfel_detector.h"
#include <iostream>


namespace srrg_matchable_detector {

  using namespace srrg_matchable;

  SurfelDetector::SurfelDetector() {
    _initialized = false;
  }

  SurfelDetector::~SurfelDetector() {
  }


  void SurfelDetector::init() {
    std::cerr << FGRN("[SurfelDetector] init:") << std::endl;
    std::cerr << "Configuration: " << std::endl;
    std::cerr << "\t>>voxel_size: " << _configuration.voxel_size << std::endl;
  
    _initialized = true;   
  }

  void SurfelDetector::compute(Scene &matchables_,
                               const srrg_core::Float3Image& points_image_,
                               const srrg_core::Float3Image& normals_image_,
                               const srrg_core::IntImage& regions_image_) {
    if(!_initialized)
      throw std::runtime_error("[SurfelDetector][compute]: not initialized");

    const int rows = regions_image_.rows;
    const int cols = regions_image_.cols;

    srrg_core::Cloud3D cloud;
    cloud.resize(rows*cols);
    int k=0;
    const int skip = 20;
    
    for(int r = skip; r < rows - skip; ++r) {
      const int* region = regions_image_.ptr<const int>(r) + skip;
      const cv::Vec3f* point = points_image_.ptr<const cv::Vec3f>(r) + skip;
      const cv::Vec3f* normal = normals_image_.ptr<const cv::Vec3f>(r) + skip;
      for(int c = skip; c < cols - skip; ++c, ++region, ++point, ++normal) {
        if(*region != PlaneDetector::Surfel)
          continue;
        if(fabs(cv::norm(*normal)) < 1e-3f)
          continue;
        if(fabs(cv::norm(*point)) < 1e-3f)
          continue;
        cloud[k] = srrg_core::RichPoint3D(Eigen::Vector3f((*point)[0], (*point)[1], (*point)[2]),
                                          Eigen::Vector3f((*normal)[0], (*normal)[1], (*normal)[2]),1.0);
        ++k; 
      }
    }

    if(k) {
      cloud.resize(k);
      cloud.voxelize(_configuration.voxel_size);
      for(const srrg_core::RichPoint3D& rich : cloud){
        MatchablePtr surfel(new Matchable(Matchable::Surfel, rich.point()));
        surfel->setRotation(rich.normal());
        matchables_.push_back(surfel);
      }
    }
    

    
  }
}
