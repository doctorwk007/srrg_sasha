#include "plane_detector.h"
#include <map>

using namespace std;
using namespace srrg_core;
using namespace srrg_matchable;

namespace srrg_matchable_detector{

  const float FLOAT_MIN = 1e-6f;

  PlaneDetector::PlaneDetector(){
    _initialized = false;
  }

  void PlaneDetector::init(){
    std::cerr << FGRN("[PlaneDetector] init") << std::endl;
    std::cerr << "Configuration: " << std::endl;
    std::cerr << "\t>>Min Cluster Points: " << _configuration.min_cluster_points << std::endl;
    _initialized = true;
  }
  
  void PlaneDetector::extractClusters(ClustererPathSearch::ClusterVector& clusters,
                                      const srrg_core::IntImage& regions_image_) {

    ClustererPathSearch clusterer;
    PathMap output_map;
    clusterer.setOutputPathMap(output_map);
    clusterer.setRegionsImage(regions_image_);
    int color = rand()*16777215;
    clusterer.setColor(color);
    clusterer.init();
    clusterer.compute();

    clusters = clusterer.clusters();
    
  }

  void PlaneDetector::computePlaneRotationMatrix(Eigen::Matrix3f& rotation_matrix,
                                                 const Eigen::Vector3f& direction){
    
    float d = sqrt(direction.x()*direction.x() + direction.y()*direction.y());
    
    const float& dirx = direction.x();
    const float& diry = direction.y();
    const float& dirz = direction.z();
    
    if(d > std::numeric_limits<float>::min()) {
      rotation_matrix << diry/d,     dirx*dirz/d,     dirx,
        -dirx/d,   diry*dirz/d, diry,
        0,          -d,        dirz;
    } else {
      rotation_matrix.setIdentity();
    }
    
  }


  void PlaneDetector::generatePlanefromCluster(srrg_matchable::Scene& matchables_,
                                               srrg_core::IntImage& regions_image,
                                               const srrg_core::Float3Image& points_image,
                                               const srrg_core::Float3Image& normals_image,
                                               const ClustererPathSearch::Cluster& cluster_) {

    Eigen::Vector3f point_ = Eigen::Vector3f::Zero();
    Eigen::Vector3f direction_vector_ = Eigen::Vector3f::Zero();

    if(cluster_.point_count < _configuration.min_cluster_points) {
      //std::cerr << "[PlaneDetector][Fitting]: rejected due to few points: " << cluster_.point_count << std::endl;
      return;
    }

    const Vector2iVector& pixels = cluster_.pixels;
    const int pixels_size = pixels.size();

    if(cluster_.point_count != pixels_size+1)
      throw std::runtime_error("cunnilingus");

    // Compute centroid and Direction Vector
    float x_min = std::numeric_limits<float>::max(),y_min = std::numeric_limits<float>::max(),z_min = std::numeric_limits<float>::max();
    float x_max = -std::numeric_limits<float>::max(),y_max = -std::numeric_limits<float>::max(),z_max = -std::numeric_limits<float>::max();

    Cloud3D point_cloud;
    point_cloud.resize(pixels_size);
    int k=0;

    for(size_t i = 0; i < pixels_size; ++i) {
      const cv::Vec3f& cv_point = points_image.at<const cv::Vec3f>(pixels[i].x(), pixels[i].y());
      const cv::Vec3f& cv_normal = normals_image.at<const cv::Vec3f>(pixels[i].x(), pixels[i].y());
      int& region = regions_image.at<int>(pixels[i].x(), pixels[i].y());
      
      if(cv::norm(cv_point) < 1e-3 || cv::norm(cv_normal) < 1e-3)
        continue;

      region = PixelType::Plane;
      
      Eigen::Vector3f current_point(cv_point[0], cv_point[1],cv_point[2]);
      Eigen::Vector3f current_normal(cv_normal[0], cv_normal[1],cv_normal[2]);
      point_cloud[k] = RichPoint3D(current_point,current_normal,1.0f);
      k++;

      if(current_point.x() < x_min)
        x_min = current_point.x();
      if(current_point.x() > x_max)
        x_max = current_point.x();
      if(current_point.y() < y_min)
        y_min = current_point.y();
      if(current_point.y() > y_max)
        y_max = current_point.y();
      if(current_point.z() < z_min)
        z_min = current_point.z();
      if(current_point.z() > z_max)
        z_max = current_point.z();
      
      point_ += current_point;
      direction_vector_ += current_normal;
    }

    if(!k)
      return;

    point_cloud.resize(k);
    
    point_ /= (float)k;
    direction_vector_ /= (float)k;
    direction_vector_.normalize();

    //plane centroid
    Eigen::Vector3f median((x_max+x_min)*.5f,
                           (y_max+y_min)*.5f,
                           (z_max+z_min)*.5f);

    //plane transform
    Eigen::Matrix3f R;
    computePlaneRotationMatrix(R,direction_vector_);
    Eigen::Isometry3f transform = Eigen::Isometry3f::Identity();
    transform.linear() = R;
    transform.translation() = median;
    transform = transform.inverse();

   
    MatchablePtr matchable(new Matchable(Matchable::Plane,median));
    matchable->setRotation(direction_vector_);
    point_cloud.transformInPlace(transform);
    //point_cloud.voxelize(0.05f);
    matchable->setCloud(point_cloud);
    matchable->voxelizeCloud();
    matchable->setRotationMatrix(R);
    
    matchables_.push_back(matchable);
  }

  void PlaneDetector::compute(Scene& matchables_,
                              srrg_core::IntImage& regions_image,
                              const srrg_core::Float3Image& points_image,
                              const srrg_core::Float3Image& normals_image,
                              const srrg_core::FloatImage& curvature_image) {
    assert(_initialized && "mulier equitans");
    
    int rows = normals_image.rows;
    int cols = normals_image.cols;
    
    ClustererPathSearch::ClusterVector clusters;
    extractClusters(clusters, regions_image);

    //std::cerr << "Extracted " << clusters.size() << " clusters" << std::endl;
    
    // Generating Planes from clusters
    for(size_t c=0; c < clusters.size(); ++c)  
      generatePlanefromCluster(matchables_,
                               regions_image,
                               points_image,
                               normals_image,
                               clusters[c]);
  }
}
