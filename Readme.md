# SA-SHA: Scene Alignment with Shapes

Registering models is an essential building block of
many robotic applications. In case of 3D data, the models to be
aligned usually consist of point clouds. In this work we propose
a formalism to represent in a uniform manner scenes consisting
of high-level geometric primitives, including lines and planes.
Additionally, we derive both an iterative and a direct method
to determine the transformation between heterogeneous scenes
(solver). We analyzed the convergence behavior of this solver
on synthetic data. Furthermore, we conducted comparative
experiments on a full registration pipeline that operates on raw
data, implemented on top of our solver. To this extent we used
public benchmark datasets and we compared against state-of-
the-art approaches. 

## Supplementary Material

Supplementary material for this research work can be found [here](https://gitlab.com/srrg-software/srrg_sasha/blob/master/latex/2018_nardi-dellacorte_ral_primitives_registration_sup.pdf).

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

## Installation

### Prerequisites

* [srrg_cmake_modules](https://gitlab.com/srrg-software/srrg_cmake_modules) - _collection of cmake modules_
* [srrg_core](https://gitlab.com/srrg-software/srrg_core) - _core, i.e. low-level algorithms and utilities_
* [srrg_core_viewers](https://gitlab.com/srrg-software/srrg_core_viewer) - _low-level viewer and utilities_
* [srrg_gl_helpers](https://gitlab.com/srrg-software/srrg_gl_helpers) - _low-level openGL utilities_

There is a couple of ways how to get these:

### Get dependencies automatically
The easiest way is to use `catkin deps fetch` command from package
[catkin_tools_fetch](https://github.com/Photogrammetry-Robotics-Bonn/catkin_tools_fetch):
```bash
sudo apt-get install python-pip                                              # install pip
sudo pip install catkin_tools_fetch                                          # install catkin_tools_fetch

cd catkin_ws                                                                 # navigate to the catkin workspace
git clone https://gitlab.com/srrg-software/srrg_sasha src/srrg_sasha         # clone this repo
catkin deps fetch --default_url https://gitlab.com/srrg-software srrg_sasha  # fetch its dependencies
catkin build                                                                 # build all packages
```

It will clone the required repos and build everything you need.


### [alternative] Get dependencies manually

Clone the following repos in your catkin workspace

```bash
cd catkin_ws/src  # navigate to the catkin workspace
git clone https://gitlab.com/srrg-software/srrg_cmake_modules
git clone https://gitlab.com/srrg-software/srrg_core
git clone https://gitlab.com/srrg-software/srrg_core_viewers
git clone https://gitlab.com/srrg-software/srrg_gl_helpers
git clone https://gitlab.com/srrg-software/srrg_sasha
cd ..         # return to catkin workspace root
catkin build  # build all packages
```

## Run the code on test data

### Test 1 - Synthetic app

    rosrun srrg_sasha srrg_synthetic_registration_app

By selecting the `GUI` the following interactions are available:

|   **Key**    |           **Action**          |   **Key**    |           **Action**            |  
| :----------- | :-----------------------------| :----------- | :-------------------------------| 
|    `[1]`     | add _Point2Point constraint_  |   `[DEL]`    |       _clear_ Scenes            | 
|    `[2]`     | add _Point2Line constraint_   |    `[S]`     |_switch Solver_{iterative/direct}| 
|    `[3]`     | add _Point2Plane constraint_  |    `[X]`     |           _init_                |
|    `[4]`     | add _Point2Surfel constraint_ |  `[SPACE]`   |     _one Round_ optimization    |
|    `[5]`     | add _Line2Point constraint_   |    `[D]`     |     draw _Direction Matrix_     |
|    `[6]`     | add _Line2Line constraint_    |    `[N]`     |       change _noise_ level      |
|    `[7]`     | add _Line2Plane constraint_   |    `[U]`     |       print cumulative error    |
|    `[8]`     | add _Line2Surfel constraint_  |    `[H]`     |          print this Help        |
|    `[9]`     | add _Plane2Point constraint_  | 
|    `[0]`     | add _Plane2Line constraint_   | 
|    `[Q]`     | add _Plane2Plane constraint_  |
|    `[W]`     | add _Plane2Surfel constraint_ |
|    `[E]`     | add _Surfel2Point constraint_ | 
|    `[R]`     | add _Surfel2Line constraint_  | 
|    `[T]`     | add _Surfel2Plane constraint_ |
|    `[Y]`     | add _Surfel2Surfel constraint_|

### Test 2 - ICL-NUIM simulated data

**1.** download the ICL-NUIM  sequence [_living_room_0_](https://drive.google.com/file/d/11V4_M89_gfKOybNUU1AY-SSSflQuTYcY/view?usp=sharing) (size 760MB) into a folder on your computer

**2.** launch a terminal in the folder and uncompress the tarball

`tar -xzvf ICL_living_room_traj0_frei_png.tar.gz`

**3.** run the example directly in the folder

`cd ICL_living_room_traj0_frei_png`

`rosrun srrg_sasha srrg_sasha_tracker_app -t /camera/depth/image -rgbt /camera/rgb/image_color -ns living_room_traj0_frei_png.txt`

Select the GUI and press `P` to run the tracker. Typing `rosrun srrg_sasha srrg_sasha_tracker_app -h` will show the available options

![](https://media.giphy.com/media/2xPVfb5qkAnEHGMqhb/giphy.gif)

### Test 3 - TUM real data

**1.** download the TUM sequence [_fr2/desk_](https://drive.google.com/file/d/1_5Sol_S45-T6ZL2EKsZvxAt9SeKnJD1z/view?usp=sharing) (size 1.8GB) into a folder on your computer

**2.** launch a terminal in the folder and uncompress the tarball

`tar -xzvf TUM_rgbd_dataset_freiburg2_desk.tar.gz`

**3.** run the example directly in the folder

`cd TUM_rgbd_dataset_freiburg2_desk`

`rosrun srrg_sasha srrg_sasha_tracker_app -t /camera/depth/image -rgbt /camera/rgb/image_color -config config.yaml -ns rgbd_dataset_freiburg2_desk.txtio`

Select the GUI and press `P` to run the tracker. Typing `rosrun srrg_sasha srrg_sasha_tracker_app -h` will show the available options

![](https://media.giphy.com/media/Zyj6Hf9y9mr2HsyYE1/giphy.gif)

## Authors

* Federico Nardi
* Bartolomeo Della Corte
* Giorgio Grisetti

## License and credits

This project is licensed under the FreeBSD License - see the LICENSE file for details.
